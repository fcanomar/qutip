
from qutip import *
from pylab import *

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)

# Two-Level System

E = 10

zero = basis(2,0) # |s> = |0>
one = basis(2,1) # |1>

# Harmonic Oscillator

N = 3 # for example truncate at level 3
w = 0.1
# I set h_bar equal to one

a = destroy(N)

hzero = basis(N,0) # |n> = |0>
hone = basis(N,1) # |1>
htwo = basis(N,2) # |2>


# Composite System : Two-Level System + Harmonic Oscillator

V=1

v1 = tensor(hzero, zero) # |ns> = |00>
v2 = tensor(hzero, one) # |01>
v3 = tensor(hone, zero) # |10>
v4 = tensor(hone, one) # |11>
v5 = tensor(htwo, zero) # |20>
v6 = tensor(htwo, one) # |21>

zero

one

hzero

basis(N,1)

a.dag() * hzero

basis(N,2)

a.dag() * hone == sqrt(2) * htwo



psi0 = tensor(1/sqrt(2) * (zero + one), hzero)
psi0

rho0 = ket2dm(psi0)
rho0

rho0 == psi0 * psi0.dag()

# times = linspace(0.0, 10.0, 20.0)
# result = mesolve(H, rho0, times, [], [])
# result.states

## no resuelve por incompatibilidad de objetos

E = w

# Space of States :

# Harmonic Oscillator

N = 2 # for example truncate at level 3
w = 0.1
# I set h_bar equal to one

a = destroy(N)

hzero = basis(N,0) # |n> = |0>
hone = basis(N,1) # |1>


# Composite System : Two-Level System + Harmonic Oscillator

V=1

v1 = tensor(hzero, zero) # |ns> = |00>
v2 = tensor(hzero, one) # |01>
v3 = tensor(hone, zero) # |10>
v4 = tensor(hone, one) # |11>

# Hamiltonian :

H = 0 * tensor(qeye(2),zero * zero.dag()) + E * tensor(qeye(2), one * one.dag()) + V * ( tensor(a.dag(),zero * one.dag()) + tensor(a, one * zero.dag())) + tensor( w * (a.dag() * a + .5), qeye(2))
H


H.eigenstates()

times = linspace(0.0, 10.0, 20.0)
psi0 = tensor(hzero, 1/sqrt(2) * (zero + one))
rho0 = ket2dm(psi0)
result = mesolve(H, ket2dm(psi0), times, [], [])
result.states
l = len(result.states)


r11 = []
for t in times:
    s = 1/4. * cos(2 * V * t) + 1/4.
    r11.append(s)

x00 = []
for i in range(l):
    s = result.states[i][0,0]
    x00.append(s)

x22 = []
for i in range(l):
    s = result.states[i][2,2]
    x22.append(s)

r12 = []
for t in times:
    s = 1/4. * exp(1j * t * (w - V)) + 1/4. * exp(1j * t * (w + V))
    r12.append(s)

x02 = []
for i in range(l):
    s = result.states[i][0,2]
    x02.append(s)

# Reduced Density Matrix

rho0.ptrace(1)

# Reduced Density Matrix Time Evolution

rdm = []
for i in range(l):
    s = result.states[i].ptrace(1)
    rdm.append(s)

rdm01 = []
for i in range(l):
    s = result.states[i][0,1]
    rdm01.append(s)
    
rt12 = []
for t in times:
    s = 1/4. * exp(1j * t * (w - V)) + 1/4. * exp(1j * t * (w + V))
    rt12.append(s)
    
max(array(rdm01) - array(rt12)) < 0.001
    
plt.plot(times, rdm01)