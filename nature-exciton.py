#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 15:10:12 2017

@author: fcm
"""

# runfile('/Users/fcm/qutip/nature-protein.py', wdir='/Users/fcm/qutip')

w = [wa, wb]

A = [a1, a2]

e1 = tensor(one, zero, zero, zero)
e2 = tensor(zero, one, zero, zero)

e =[e1, e2]


# HAMILTONIAN IN SITE BASIS

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()
    

# excitons

exc1 = HS.eigenstates()[1][0]
E1 = HS.eigenstates()[0][0]
exc2 = HS.eigenstates()[1][15]
E2 = HS.eigenstates()[0][15]

HS * exc1 == E1 * exc1
HS * exc2 == E2 * exc2

exc = [exc1, exc2]
E = [E1, E2]  


# |1> ie. |10> coefficients

c11 = exc1.dag() * e1
c12 = exc2.dag() * e1
              
# |2> ie. |01> coefficients
              
c21 = exc1.dag() * e2              
c22 = exc2.dag() * e2       
              
C = array([[c11, c12],[c21, c22]])              

# single excitation vectors in exciton basis              
              
e1 == c11 * exc1 + c12 * exc2
e2 == c21 * exc1 + c22 * exc2

# S and Q

S11 = sqrt(sqrt(JJ(wa)/wa))
S12 = sqrt(sqrt(JJ(wb)/wb))
S21 = S11
S22 = S12

S = array([[S11, S12], [S21, S22]])

def Q(n,m):
    Q = 0
    for i in range(N):
        for k in range(2):
            Q = sqrt(S[i,k]) * w[k] * C[i,n] * C[i,m] * (A[k] + A[k].dag())
    return Q

# HAMILTONIAN IN EXCITON BASIS

HSE = 0
for n in range(N):
    HSE += E[i] * exc[i] * exc[i].dag()
    for m in range(N):
        HSE += Q(n,m) * e[n] * e[m].dag()
    
HE = HSE + HB