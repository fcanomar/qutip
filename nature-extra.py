#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 22 17:50:56 2017

@author: fcm
"""

zero = basis(2,0)
one = basis (2,1)
vac = Qobj([[0],[0]])

a = destroy(2)
I = qeye(2)


#################################################################
# Single Harmonic Oscillator 
#################################################################

w = 3

H1 = w * a.dag() * a

# following statements are true
a.dag() * zero == one
a * zero == vac
a.dag() * one == vac
a * one == zero    

# Eigenstates

# one eigenstate with eigenvalue zero
   

#################################################################
# System of Two Harmonic Oscillators 
#################################################################

w1 = 1
w2 = 5

a1 = tensor(a,I)
a2 = tensor(I,a)

H2 = w1 * a1.dag() * a1 + w2 * a2.dag() * a2
                
# Eigenstates  

H2 * tensor(zero, zero) == tensor(vac, vac)   
H2 * tensor(one, zero) == w1 * tensor(one,zero)           
H2 * tensor(zero,one) == w2 * tensor(zero,one)
H2 * tensor(one, one)  == (w1 + w2) * tensor(one, one)               


#######################################################################################
# System of Two Chromophores and Two Harmonic Oscillators : Oscillators Hamiltonian
#######################################################################################


# Harmonic Oscillator N=2

a1 = tensor(I, I, a, I)
a2 = tensor(I, I, I, a)

HB =   w1 * a1.dag() * a1 + w2 * a2.dag() * a2
                  

e1 = tensor(zero, zero, zero, one) # |0001>
e2 = tensor(zero, zero, one, zero) # |0010> 


HB * e1 == w1 * e1 # this is true
HB * e2 == w2 * e2 # true


