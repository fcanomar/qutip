#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 14:09:29 2017

@author: fcm
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 17:31:10 2017

Nature Dimer Coupled to Bath - Pauli Matrices Basis 4

@author: fcm
"""

from qutip import *
from pylab import *
from scipy import *
from math import factorial
import sympy
import os

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)


# working directory

os.chdir('/Users/fcm/qutip/figures/basis4')


# state space

# |0> and |1> states 
zero = basis(2,0)
one = basis (2,1)

e1 = tensor(one, zero)
e2 = tensor(zero, one)

e = [e1,e2]


# parameters

eps = [10, 130]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])


# spectral densities

ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22

def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ


# mode displacement operator

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )


# HAMILTONIAN

zero = basis(2,0)
one = basis(2,1)

eps1 = eps[0]
eps2 = eps[1]
J12 = J[0,1]

# Single Excitation Basis


I = tensor(qeye(2), qeye(2))
sz1 = tensor(sigmaz(), qeye(2))
sz2 = tensor(qeye(2), sigmaz())


HS2 = 0.5 * (eps1 + eps2) * qeye(2) + .5 * (eps1 - eps2) * sigmaz() + J12 * sigmax()


I = qeye(2)
a1 = tensor(a, I)
a2 = tensor(I, a)


g1 = sqrt(JJ(wa))
g2 = sqrt(JJ(wb))
#g1 = 0.001
#g2 = 0.002


X = g1 * ( a1 + a1.dag() ) + g2 * ( a2 + a2.dag() )


HI2 = X


HB2 =   2 * (wa * a1.dag() * a1 + wb * a2.dag() * a2)
    
              
H2 = tensor(HS2, tensor(qeye(2), qeye(2))) + tensor(qeye(2),HI2) + tensor(qeye(2),HB2)


# System's hamiltonian in |00> |01> ... basis

e1 = tensor(zero, one)
e2 = tensor(one, zero)


HS = eps1 * e1 * e1.dag() + eps2 * e2 * e2.dag() + J12 * (e1 * e2.dag() + e2 * e1.dag())

HI = tensor(e1 * e1.dag() + e2 * e2.dag(), X)

# HI = tensor(e1 * e2.dag(),a1 + a2) + tensor(e2 * e1.dag(), a1.dag() + a2.dag())

HB =   2 * (wa * a1.dag() * a1 + wb * a2.dag() * a2)

H = tensor(HS, tensor(qeye(2),qeye(2))) + HI + tensor(tensor(qeye(2),qeye(2)), HB)

###################################################################################


# time evolution starting in one localized excitation

psi0 = tensor(one,zero)
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)

# sigmaz

times = linspace(0.0, 10.0e-1, 600.0)
result = mesolve(H, rho0, times, [], [tensor(e1 * e1.dag(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig('sigmaz_localized.png')
show()



# coherence in rdm

result = mesolve(H, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho00_rdm_localized.png')
show() 


result = mesolve(H, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,2].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_12 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho01_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,1].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_21 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho10_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,2]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_22 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show()   
  



# time evolution starting in triplet state

psi0 = 1/sqrt(2) * (tensor(zero,one) + tensor(one,zero))
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)

# sigmaz

times = linspace(0.0, 10.0e-1, 600.0)
result = mesolve(H, rho0, times, [], [tensor(e1 * e1.dag(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig('sigmaz_localized.png')
show()



# coherence in rdm

result = mesolve(H, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho00_rdm_localized.png')
show() 


result = mesolve(H, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,2].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_12 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho01_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,1].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_21 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho10_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,2]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_22 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show()   
  
  
# time evolution starting in singlet state

psi0 = 1/sqrt(2) * (tensor(zero,one) - tensor(one,zero))
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)

# sigmaz

times = linspace(0.0, 10.0e-1, 600.0)
result = mesolve(H, rho0, times, [], [tensor(e1 * e1.dag(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig('sigmaz_localized.png')
show()



# coherence in rdm

result = mesolve(H, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho00_rdm_localized.png')
show() 


result = mesolve(H, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,2].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_12 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho01_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,1].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_21 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho10_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,2]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_22 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show()   
  