#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 10 16:25:56 2018

Potential Plots for FSSH

@author: fcm
"""

import numpy as np
import math as m
import matplotlib.pyplot as plt

a = 0.01
b = 1.6
c = 0.005
d = 1.

x_vector = np.arange(-10, 10, 0.5)

#v11 = m.copysign(a, x) * ( 1.0 - m.exp(-b * abs(x)) )
#v22 = -v11
#v12 = c * m.exp(-d * x * x)
#out = np.array([ [v11, v12],
#                [v12, v22] ])

pot = []
v11_vect = []
v12_vect = []
v22_vect = []
energies_vect = []
coeff_vect = []

for x in x_vector:
    v11 = m.copysign(a, x) * ( 1.0 - m.exp(-b * abs(x)) )
    v22 = -v11
    v12 = c * m.exp(-d * x * x)
    out = np.array([ [v11, v12],
                [v12, v22] ])
    
    energies, coeff = np.linalg.eigh(out)
    
    pot.append(out)
    v11_vect.append(v11)
    v12_vect.append(v12)
    v22_vect.append(v22)
    energies_vect.append(energies)
    coeff_vect.append(coeff)
    
    
    
plt.plot(x_vector,v11_vect)
# plt.plot(x_vector,v12_vect)
plt.plot(x_vector,v22_vect)
plt.plot(x_vector, energies_vect)