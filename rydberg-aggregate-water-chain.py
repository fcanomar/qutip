#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Sep 30 16:12:37 2018

Rydberg Aggregate Linear Chain

@author: fcm
"""

# I consider a linear chain of N Rydberg atoms

from pylab import *
import numpy as np

from qutip import *

import scipy.sparse as sp

import sys
sys.setrecursionlimit(1000000)


# I consider a linear chain of N Rydberg atoms capable of exhibiting collective eigenstates

N = 5

x = [0,1,6,9,12] # atoms positions

si = qeye(2); sp = sigmap(); sm = sigmam()

sp_list = []; sm_list = [];
for n in range(N):
    op_list = [si for m in range(N)]
    op_list[n] = sp
    sp_list.append(tensor(op_list))
    op_list[n] = sm
    sm_list.append(tensor(op_list))
    
# I construct Hamiltonian

C3 = 1.0

H = 0
for n in range(N):
    for m in range(N):
        if n != m :     
            H += C3 * ((np.abs(x[n] - x[m])) ** -3.0) * (sp_list[n] * sm_list[m])   
            
# I get eigenvectors   
            
# H.eigenstates()

# Basis Vectors

g = basis(2,1)
gg = tensor(g,g,g,g,g)

# for n in range(N):
#    e_list = [gg for m in range(N)]
#    e_list[n] = sp_list[n] * gg

# sm_list[0] * gg

print(gg)
print(sp_list[0] * gg)
print(sm_list[0] * gg)     

e0 = sp_list[0] * gg
e1 = sp_list[1] * gg
e2 = sp_list[2] * gg
e3 = sp_list[3] * gg
e4 = sp_list[4] * gg

print(e0.dag())
print(H.eigenstates()[1][0])

print('n=0, k=0..4')
print(e0.dag() * H.eigenstates()[1][0])
print(e0.dag() * H.eigenstates()[1][1])
print(e0.dag() * H.eigenstates()[1][2])
print(e0.dag() * H.eigenstates()[1][3])
print(e0.dag() * H.eigenstates()[1][4])

plt.plot()

print('n=1..4')
print(e1.dag() * H.eigenstates()[1][0])
print(e2.dag() * H.eigenstates()[1][0])
print(e3.dag() * H.eigenstates()[1][1])
print(e4.dag() * H.eigenstates()[1][0])

# print(e1.dag() * H.eigenstates()[1][1])

n=0
m=1
print(x[n])
print(x[m])
print(np.abs(x[n] - x[m]))

v = e1.dag() * H.eigenstates()[1][0]
print('Getting v...')
print(v[0][0])

import matplotlib.pyplot as plt

exc_matrix = np.zeros((32, N)); 

for n in range(32):
    for k in range(N):
        pi_k = sp_list[k] * gg
        phi_n = H.eigenstates()[1][n]
        p = pi_k.dag() * phi_n
        exc_matrix[n,k] =  p[0][0][0]

exc_matrix     
# exc_matrix[4,]        
plt.hist(exc_matrix[4,])      




#################################################################################### 
# N equals 3
#################################################################################### 


N = 3
C3 = 1.0

x = [0,1,6] # atoms positions

si = qeye(2); sp = sigmap(); sm = sigmam()

sp_list = []; sm_list = [];
for n in range(N):
    op_list = [si for m in range(N)]
    op_list[n] = sp
    sp_list.append(tensor(op_list))
    op_list[n] = sm
    sm_list.append(tensor(op_list))
    
H = 0
for n in range(N):
    for m in range(N):
        if n != m :     
            H += C3 * ((np.abs(x[n] - x[m])) ** -3.0) * (sp_list[n] * sm_list[m])

H 

g = basis(2,1)
gg = tensor(g,g,g)


pi0 = sp_list[0] * gg
pi1 = sp_list[1] * gg
pi2 = sp_list[2] * gg

print(pi0)
print(pi1)
print(pi2)

exc_matrix = np.zeros((N, 8)); 

for n in range(8):
    for k in range(N):
        pi_k = sp_list[k] * gg
        phi_n = H.eigenstates()[1][n]
        p = pi_k.dag() * phi_n
        exc_matrix[k,n] =  p[0][0][0]

exc_matrix 


plt.hist(exc_matrix[0,]) 
plt.hist(exc_matrix[1,]) 
plt.hist(exc_matrix[2,]) 

#################################################################################### 
# N equals 5
#################################################################################### 


N = 5
C3 = 1.0

x = [0,3,6,9,12,15] # atoms positions

si = qeye(2); sp = sigmap(); sm = sigmam()

sp_list = []; sm_list = [];
for n in range(N):
    op_list = [si for m in range(N)]
    op_list[n] = sp
    sp_list.append(tensor(op_list))
    op_list[n] = sm
    sm_list.append(tensor(op_list))
    
H = 0
for n in range(N):
    for m in range(N):
        if n != m :     
            H += C3 * ((np.abs(x[n] - x[m])) ** -3.0) * (sp_list[n] * sm_list[m])

H 

g = basis(2,1)
gg = tensor(g,g,g,g,g)


pi0 = sp_list[0] * gg
pi1 = sp_list[1] * gg
pi2 = sp_list[2] * gg

print(pi0)
print(pi1)
print(pi2)

exc_matrix = np.zeros((N, 32)); 

for n in range(32):
    for k in range(N):
        pi_k = sp_list[k] * gg
        phi_n = H.eigenstates()[1][n]
        p = pi_k.dag() * phi_n
        exc_matrix[k,n] =  p[0][0][0]

exc_matrix 


plt.hist(exc_matrix[0,]) 
plt.hist(exc_matrix[1,]) 
plt.hist(exc_matrix[2,]) 
plt.hist(exc_matrix[3,]) 
plt.hist(exc_matrix[4,]) 

#################################################################################### 
# Single Excitation Case
#################################################################################### 

N = 5
C3 = 1.0

e0 = basis(5,0)
e1 = basis(5,1)
e2 = basis(5,2)
e3 = basis(5,3)
e4 = basis(5,4)

ee = [e0,e1,e2,e3,e4]

x = [0,3,6,9,12] # atoms positions

si = qeye(2); sp = sigmap(); sm = sigmam()

sp_list = []; sm_list = [];
for n in range(N):
    op_list = [si for m in range(N)]
    op_list[n] = sp
    sp_list.append(tensor(op_list))
    op_list[n] = sm
    sm_list.append(tensor(op_list))
    
H = 0
for n in range(N):
    for m in range(N):
        if n != m :    
            pi_n = ee[n]
            pi_m = ee[m]
            H += C3 * ((np.abs(x[n] - x[m])) ** -3.0) * (pi_n * pi_m.dag())

exc_matrix = np.zeros((N, N)); 

for n in range(N):
    for k in range(N):
        pi_k = ee[k]
        phi_n = H.eigenstates()[1][n]
        p = pi_k.dag() * phi_n
        exc_matrix[n,k] =  p[0][0][0]

exc_matrix 

n = [i for i in range(0,5)]

plt.bar(n, exc_matrix[0,])
plt.bar(n, exc_matrix[1,])
plt.bar(n, exc_matrix[2,])
plt.bar(n, exc_matrix[3,])
plt.bar(n, exc_matrix[4,])

# Time Evolution

rho0 = 1/sqrt(5)*(e0 + e1 + e2 + e3 + e4)
# rho0 = H.eigenstates()[1][0]
times = linspace(0.0, 100.0, 1000.0)
result = mesolve(H, rho0, times, [], [])

l = len(result.states)

z = []
for i in range(l):
    si = result.states[i][3][0]
    s = si.real
    z.append(s)

plt.plot(times, z)
plt.ylabel('Excitation on fist atom')
plt.xlabel('Time')
#savefig('rho23_' + tag)
show() 

