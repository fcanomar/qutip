#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 13:26:20 2019

Potentials and Pseudopotentials of the Water Dimer - Single Active Electron Approximation

@author: fcm
"""

from ase.units import Bohr
from gpaw import GPAW, PW
from ase import Atoms
from gpaw.external import ExternalPotential
from ase.data import s22
from ase.md import Langevin
from ase.io.trajectory import Trajectory
import ase.units as units
import math
import numpy as np
import matplotlib.pyplot as plt

from ase.visualize import view


#====================================================================================================================================================#
# Create Dimer and place in Cell
#====================================================================================================================================================#

a = 4.0

# Dataset reference: P. Jurecka, J. Sponer, J. Cerny, P. Hobza; Phys Chem Chem Phys 2006, 8 (17), 1985-1993
# pos = [[-1.551007, -0.114520, 0.000000],
#       [-0.599677, 0.762503, 0.000000],
#       [-0.599677, 0.040712 , 0.000000],
#       [1.350625, 0.111469,  0.000000],
#       [1.680398,  -0.373741,  -0.758561],
#       [1.680398,  -0.373741,   0.758561]
#       ]

# atoms = Atoms('OH2OH2', positions=pos)

atoms = s22.create_s22_system('Water_dimer')

atoms.set_cell((2*a,a,a))
atoms.center()

# Rotate atoms to align 0-H..O to x axis

rp = [1,0,0]
v1 = atoms.get_positions()[0]
v2 = atoms.get_positions()[3]
v = (v2 - v1)/np.linalg.norm(v2 - v1)

axis = np.cross(v,rp)
angle = np.arccos(np.dot(rp,v))
    
atoms.rotate(axis,angle,center=atoms.get_positions()[0]) 

pos = atoms.get_positions() 

#====================================================================================================================================================#
# Set Calculator and Initiate
#====================================================================================================================================================#

calc = GPAW(txt='dimer-sae.txt')
atoms.calc = calc

# Get Potential Energy 

atoms.get_potential_energy()


#====================================================================================================================================================#
# Calculate Vnn(R)
#====================================================================================================================================================#

# I am use B.O. approximation to calculate Vnn(R)

r = np.linspace(-0.15,0.15,15)

x_H = atoms.get_positions()[2][1]

R = []
V = []

for x_i in r: 

    x = x_H + x_i
    pos[2][1] = x
    
    atoms.set_positions(pos)
    
    V_x = atoms.get_potential_energy()
    
    R.append(x)
    V.append(V_x)


plt.plot(R,V)

