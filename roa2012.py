#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Created on Sun Mar 11 14:15:44 2018

Three-level system
bosonic environment
driven by two Raman fields 
(Roa 2012)

@author: fcm
"""

from qutip import *
from pylab import *

# show whole vectors 
import numpy as np
# numpy.set_printoptions(threshold=numpy.nan)

import scipy.sparse as sp

import sys
sys.setrecursionlimit(10000)

# INITIALLY ONLY ONE MOLECULE

# Parameters

N = 2 # number of molecules
M = 5 # number of bosonic states

tag = '5molecules'

delta  = 0.1
omega = 0.1


w = 1.0
g1 = 0.01
g2 = 0.01
sigma1 = 1.0
sigma2 = 1.0

wl = 0.1
wj_list = [w,w,w]
wk_list = [w,w,w,w,w]

# hamitonian basis for N=3

grn = basis(3,0) # ground state |g>
med = basis(3,1) # medium state |m>
exc = basis(3,2) # excited state |e>

med_list = []
exc_list = []
grn_list = []

for i in range(N):
    op_list = [grn for m in range(N)]
    grn_list.append(tensor(op_list))

for i in range(N):
    op_list = [grn for m in range(N)]
    op_list[i] = med
    med_list.append(tensor(op_list))
    
for i in range(N):
    op_list = [grn for m in range(N)]
    op_list[i] = exc
    exc_list.append(tensor(op_list))

mol_list = [grn_list, med_list, exc_list]

mol_eye = tensor([qeye(3) for m in range(N)])

# bosonic states

 # b_k+ 
 # b_k

a = destroy(2)

bsn_list = []; # operator list
for n in range(M):
    op_list = [qeye(2) for m in range(M)]
    op_list[n] = a
    bsn_list.append(tensor(op_list))

bsn_grn = tensor([basis(2,0) for m in range(M)])
bsn_eye = tensor([qeye(2) for m in range(M)])

# Construct the Hamiltonian

# Time-Independent Part 
    
H0 = 0

for i in range(N):
    for j in range(3):
        H0 += wj_list[j] * tensor(mol_list[j][i] * mol_list[j][i].dag(),bsn_eye)
    
for k in range(M):
    H0 += wk_list[k] * tensor(mol_eye, bsn_list[k] * bsn_list[k].dag())

for k in range(M):
    for i in range(N):
        H0 += g1 * (tensor(grn_list[i] * med_list[i].dag(), bsn_list[k].dag()) + tensor(med_list[i] * grn_list[i].dag(),bsn_list[k])) + g2 * (tensor(grn_list[i] * exc_list[i].dag(), bsn_list[k].dag()) + tensor(exc_list[i] * grn_list[i].dag(),bsn_list[k]))
       
# Time-Dependent Part

def H1_coeff(t, args):
    return np.exp(1j * wl * t)

for i in range(N):
    H1 = sigma1 * (tensor(grn_list[i] * med_list[i].dag() + med_list[i] * grn_list[i].dag(),bsn_eye)) + sigma2 * (tensor(grn_list[i] * exc_list[i].dag() + exc_list[i] * grn_list[i].dag(),bsn_eye))

# Total Hamiltonian
         
H = [H0,[H1,H1_coeff]]   




# Time Evolution
    
times = linspace(0.0, 200.0, 2000.0)

# time evolution starting in one localized excitation

psi0 = exc_list[0]
rho0_e = bsn_grn * bsn_grn.dag()
rho0 = tensor(ket2dm(psi0),rho0_e) 

# sigmaz

result = mesolve(H0, rho0, times, [], [])
   
 
# Plot Coherence

l = len(result.states)

rdm = result.states[i].ptrace([1,2])

z = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,2].real
    z.append(s)

plt.plot(times, z)
plt.ylabel('Rho_23 (Reduced Density Matrix)')
plt.xlabel('Time')
savefig('rho23_' + tag)
show() 


# Raman Field

plt.plot(times, np.exp(1j * wl * times))
plt.ylabel('Raman Field')
plt.xlabel('Time')
# savefig(sub + 'rho00_rdm_localized.png')
show() 


# Measure of Coherence: Relative Entropy of Coherence

l = len(result.states)


def erase_non_diagonal_rho(rho):
    rho_array = rho.full()
    for i in range(len(rho_array)):
        for j in range(len(rho_array)):
            if i!=j:
                rho_array[i,j]= complex(0,0)
    rho = Qobj(rho_array)                        
    return rho

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = entropy_vn(erase_non_diagonal_rho(rdm)) - entropy_vn(rdm)
    x.append(s)

plt.plot(times, x)
plt.ylabel('Relative Entropy of Coherence (Reduced Density Matrix)')
plt.xlabel('Time')
savefig('relative-entropy-coherence' + tag)
show() 


# Phase Difference

plt.plot(times, x - np.exp(1j * wl * times))
plt.ylabel('Phase Difference')
plt.xlabel('Time')
# savefig(sub + 'rho00_rdm_localized.png')
show() 


