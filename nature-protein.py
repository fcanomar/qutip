#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 17:31:10 2017

Nature Dimer Coupled to Bath

@author: fcm
"""

from qutip import *
from pylab import *
from scipy import *
from math import factorial
import sympy

# show whole vectors 
import numpy as np
numpy.set_printoptions(threshold=numpy.nan)

#dimer

N=2

# state space

# |0> and |1> states 
zero = basis(2,0)
one = basis (2,1)

e1 = tensor(one, zero, zero, zero)
e2 = tensor(zero, one, zero, zero)

e = [e1,e2]

# parameters

eps = [1, 131]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])

# spectral densities
ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22

def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ

# harmonic oscillator operators

# bath
a = destroy(2)
I = qeye(2)

a1 = tensor(I, I, a, I)
a2 = tensor(I, I, I, a)



X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )

# consider two identical harmonic oscillators each coupled to one of the sites
# we truncate to second eigenstate


# Hamiltonian

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()
    
    
HI = 0    
for i in range(N):
    HI += X * e[i] * e[i].dag()
    


HB =   wa * a1.dag() * a1 + wb * a2.dag() * a2
                  

H = HS + HI + HB


# excitons

exc1 = HS.eigenstates()[1][0]
E1 = HS.eigenstates()[0][0]
exc2 = HS.eigenstates()[1][15]
E2 = HS.eigenstates()[0][15]

HS * exc1 == E1 * exc1
HS * exc2 == E2 * exc2

exc = [exc1, exc2]
E = [E1, E2]  



# Time Evolution

#tlist = np.linspace(0, 2e-6, 100)
#
#psi0 = exc2
#
#rho0 = ket2dm(psi0)
#
#
#tlist = np.linspace(0, 2e-3, 100)
#
#psi0 = exc2
#
#rho0 = ket2dm(psi0)
#
#result = mesolve(H, rho0, tlist, [], [])
#
#rdm = []
#for i in range(l):
#    s = result.states[i].ptrace([0,1])
#    rdm.append(s)
#
#rdm12 = []
#for i in range(l):
#    s = result.states[i][1,2]
#    rdm12.append(s)

