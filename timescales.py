#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Wed Apr  4 12:54:30 2018

WATER IN NANOTUBE TIMESCALES

@author: fcm
"""

import pint
import numpy as np

ur = pint.UnitRegistry()

# Libration timescale between 10 and 100 fs, from timescales diagram corcho

t1 = 50 * ur.fs

t1.ito(ur.s)

print("TIMESCALES")
print("Libration:")
print(t1)

# Water CDs angular velocity, from "Water-mediated correlations in DNA-enzyme interactions" de Capolupo, Vitiello

h = 4.1357e-15 * ur.eV * ur.s

w0 = 1/h * 2 * np.pi / 1774 * ur.eV

# print(h)

# print("CDs - " + w0)

T0 = 1 / w0

print("CDs:")
print(T0)