#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Created on Sat Oct  6 09:00:04 2018

Surface Hopping Implementation -- Rydberg Aggregate Chain

Single Excitation Hilbert Space (see difference)

@author: fcm
"""

# Model Parameters

N = 3
C3 = 1.0


#############################################################################################
# 1. Electronic Hamiltonian for fixed *R*
#############################################################################################

# I use Single Excitation Basis, in this case it is independent of *R*

# Basis vectors

e0 = basis(3,0)
e1 = basis(3,1)
e2 = basis(3,2)

ee = [e0,e1,e2]

# *R* vector 

# x = [0.0,3.0,6.0] # atoms positions

# Hamiltonian for given *R*

def get_H(x):
    
    R = [0.0,3.0+x[0],5.0] # only with respect to one variable
    
    si = qeye(2); sp = sigmap(); sm = sigmam()

    sp_list = []; sm_list = [];
    for n in range(N):
        op_list = [si for m in range(N)]
        op_list[n] = sp
        sp_list.append(tensor(op_list))
        op_list[n] = sm
        sm_list.append(tensor(op_list))
    
    H = 0
    for n in range(N):
        for m in range(N):
            if n != m :    
                pi_n = ee[n]
                pi_m = ee[m]
                H += C3 * ((np.abs(R[n] - R[m])) ** -3.0) * (pi_n * pi_m.dag())


    return H



#############################################################################################
# 2. Orthonormal Basis of the electronic states, depending parametrically on *R*
#############################################################################################

# Adiabatic Electronic States, are the eigenstates of the Electronic Hamiltonian
    
H.eigenstates()



#############################################################################################
# 3. Matrix Elements of the Electronic Hamiltonian with respect to the basis functions
#############################################################################################



#############################################################################################
# I create a class for the FSSH program
#############################################################################################


class RydbergChain(object):
    ## Constructor that defaults to the values for the model
    def __init__(self, c3 = 0.01):
        self.c3 = c3

    ## \f$V(x)\f$
    def V(self, x):

        H = get_H(x)
        
        out = np.zeros((N,N))
        
        for i in range(0,N):
            for j in range(0,N):
                eig = H.eigenstates()
                v_ij = eig[1][i].dag() * H * eig[1][j]
                out[i,j] = v_ij[0][0][0]
        
        return out

    ## \f$\nabla V(x)\f$
    def dV(self, x):
        return self.V(x)

    def nstates(self):
        return 3

    def ndim(self):
        return 1


#############################################################################################
# Run FSSH program
#############################################################################################


chain = RydbergChain()

# fssh.TrajectorySH.box_bounds = 4 # quit the simulation once the particle leaves the box [-4,4]
fssh.TrajectorySH.nsteps = 1000 # quit the simulation after 10000 time steps

# Generates trajectories always with starting position -5, starting momentum 2, on ground state
traj_gen = fssh.TrajGenConst(-0.01, 9.0, "ground")

simulator = fssh.BatchedTraj(chain, traj_gen, samples = 20)
results = simulator.compute()
outcomes = results.outcomes

print("Probability of reflection on the ground state:    %12.4f" % outcomes[0,0])
print("Probability of transmission on the ground state:  %12.4f" % outcomes[0,1])
print("Probability of reflection on the excited state:   %12.4f" % outcomes[1,0])
print("Probability of transmission on the excited state: %12.4f" % outcomes[1,1])





