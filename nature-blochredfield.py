#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri May 26 12:46:39 2017

@author: fcm
"""
import numpy as np
import matplotlib.pyplot as plot

# I am considering my system as composed of the two sites
# then take into account coupling to bath with Bloch-Redfield formalism


w = [wa, wb]

a1 = tensor(a, I)
a2 = tensor(I, a)

A = [a1, a2]

e1 = tensor(one, zero)
e2 = tensor(zero, one)

e = [e1, e2]

# HAMILTONIAN IN SITE BASIS

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()
    

# excitons

exc1 = HS.eigenstates()[1][0]
E1 = HS.eigenstates()[0][0]
exc2 = HS.eigenstates()[1][3]
E2 = HS.eigenstates()[0][3]

HS * exc1 == E1 * exc1
HS * exc2 == E2 * exc2

exc = [exc1, exc2]
E = [E1, E2]  


# |1> ie. |10> coefficients

c11 = exc1.dag() * e1
c12 = exc2.dag() * e1
              
# |2> ie. |01> coefficients
              
c21 = exc1.dag() * e2              
c22 = exc2.dag() * e2       
              
C = array([[c11, c12],[c21, c22]])              

# single excitation vectors in exciton basis              
              
e1 == c11 * exc1 + c12 * exc2
e2 == c21 * exc1 + c22 * exc2

# S and Q

S11 = sqrt(sqrt(JJ(wa)/wa))
S12 = sqrt(sqrt(JJ(wb)/wb))
S21 = S11
S22 = S12

S = array([[S11, S12], [S21, S22]])

def Q(n,m):
    Q = 0
    for i in range(N):
        for k in range(2):
            Q = sqrt(S[i,k]) * w[k] * C[i,n] * C[i,m] * (A[k] + A[k].dag())
    return Q

# mode displacement operator

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )
X = (a + a.dag())

# HAMILTONIAN IN EXCITON BASIS

HSE = 0
for n in range(N):
    HSE += E[n] * exc[n] * exc[n].dag()
    #for m in range(N):
    #    HSE += Q(n,m) * e[n] * e[m].dag()


# HE = HSE + HB


# eigenstates

HSE * exc1 == E1 * exc1
HSE * exc2 == E2 * exc2

# TIME EVOLUTION

tlist = np.linspace(0, 2e-3, 100)

psi0 = exc2

rho0 = ket2dm(psi0)

M = exc2 * exc2.dag()

A = 0
for i in range(N):
    for n in range(N):
        for m in range (N):
            A += C[i,n] * C[i,m] * exc[n] * exc[m].dag()

A2 = exc1 * exc2.dag() + exc2 * exc1.dag()

# Unitary Evolution

#result = mesolve(H, rho0, tlist, [], [])
#
#rdm = []
#for i in range(l):
#    s = result.states[i].ptrace([0,1])
#    rdm.append(s)
#
#rdm12 = []
#for i in range(l):
#    s = result.states[i][1,2]
#    rdm12.append(s)
    

# Bloch-Redfield master equation

#R, ekets = bloch_redfield_tensor(HSE, [A2] , [JJ])
#
#e_ops = [M]
#
#expt_list = bloch_redfield_solve(R, ekets, rho0, tlist, e_ops)
#
#plt.plot(tlist, expt_list[0])