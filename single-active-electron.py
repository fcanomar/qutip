#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Created on Sun Mar 10 14:08:42 2019

Single-Active-Electron Approximation Model of an H-Bond for a Water Dimer (based on "Cavity Induced..." by Galego et al.)

@author: fcm
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.constants import hbar
from scipy import integrate
from scipy import constants
from scipy.integrate import odeint


#====================================================================================================================================================#
# Effective N-E Potential 
#====================================================================================================================================================#

Z = 6
r0 = 1.0
alpha = 1.0

def Ven(r):
    return -(.5 + (Z -1/2))*np.exp(-r/r0)/np.sqrt(r**2 + alpha**2)

# I create auxilary function that includes parameters
def Ven_par(r, Z, r0, alpha):
    return -(.5 + (Z -1/2))*np.exp(-r/r0)/np.sqrt(r**2 + alpha**2)

# Plot Potential
r = np.linspace(-5,5,10)

# y = Ven(r, Z=6, r0=1.0, alpha=1.0)
y = Ven(r)

plt.plot(r, y)
plt.show()


# Understand Ven
r0 = 1.0
r = np.linspace(0,10,10)
plt.plot(r,np.exp(-r/r0))
plt.show()

# Understand Ven
r0 = 1.0
alpha = 0.5
r = np.linspace(0,10,10)
plt.plot(r,1/np.sqrt(r**2 + alpha**2))
plt.show()

#====================================================================================================================================================#
# Effective N-N Potential
#====================================================================================================================================================#

De = 1.0
A = 1.0
R0 = 1.0

def Vnn(R):
    return De*(1-np.exp(A*(R-R0)))**2

# Plot Potential
    
r = np.linspace(-3,3,10)
y = Vnn(r)

plt.plot(r, y)
plt.show()


#====================================================================================================================================================#
# Shrödinger Equation
#====================================================================================================================================================#

# f(x) in the Shrödinger Equation Psi''(x)= f(x) * Psi(x)

E = 0.0
R = 1.0

def f(x):
    return 1/hbar*(E - Ven(np.abs(x-R/2)) - Ven(np.abs(x+R/2)) + Vnn(R))


# I transform into First-Order ODE and Solve

def model(Y, x):
    a = f(x)
    return [Y[1], a * Y[1]]

x = np.arange(-25, 25.0, 0.01)
asol = odeint(model, [0.01, 0.01], x)
print(asol)

# plot results
plt.plot(x,asol)
plt.xlabel('x')
plt.ylabel('Psi(x)')
plt.show()

