#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 10 18:48:50 2017

@author: fcm
"""

from qutip import *

eps = 100
delta = 100
gamma = 54
T = 300


sx = sigmax(); sz = sigmaz()

H = .5 * eps * sz + delta * sx 

# rest of the hamiltonian

