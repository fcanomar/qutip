from ase import Atoms
from ase.units import Hartree, Bohr

from gpaw import GPAW, PW
from gpaw.xc import XC
from gpaw.test import equal
from gpaw.xc.kernel import XCNull
from gpaw.poisson import NoInteractionPoissonSolver
from gpaw.external import ExternalPotential
from ase.data import s22
from ase.md import Langevin
from ase.io.trajectory import Trajectory



a = 4.0
# Create system
atoms = s22.create_s22_system('Water_dimer')
atoms.set_cell((2*a,a,a))
atoms.center()


class HarmonicPotential(ExternalPotential):
    def calculate_potential(self, gd):
        r_vg = gd.get_grid_point_coordinates()
        self.vext_g = 0.5 * ((r_vg - a / Bohr / 2)**2).sum(0)
        
  
d = 4 # diameter of the tube
h = 200 # well depth
      
class WellTubePotential(ExternalPotential):
    def calculate_potential(self, gd):
        r_vg = gd.get_grid_point_coordinates()
        self.vext_g = (((r_vg[1] - a / Bohr / 2)**2 + (r_vg[2] - a / Bohr / 2)**2)**.5 > d/2)*h       


calc = GPAW(external=WellTubePotential(),mode=PW(200))
# calc = GPAW(mode=PW(200))

atoms.calc = calc
atoms.get_potential_energy()






