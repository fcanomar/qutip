#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 11:59:00 2018

@author: fcm
"""

from ase.visualize import view
from ase import Atoms
from ase.md import Langevin, VelocityVerlet
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.constraints import FixBondLengths, FixedLine, FixAtoms
from ase.build import nanotube
import numpy as np
from gpaw.external import ExternalPotential
from gpaw import GPAW, PW
# import plot_potential_xy as plxy
import matplotlib.pyplot as plt
from pylab import savefig
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.calculators.tip4p import TIP4P


# ntb, ntb.calc = restart('xyz.gpw')

tag = 'water-chain-test'

gpts = (16*3, 16*3, 16*6)


#-------------------------------------------------------------------------------------------------------------------
# I set the chain of water atoms and the cell
#-------------------------------------------------------------------------------------------------------------------




# water molecule

x = angleHOH * np.pi / 180 / 2
pos = [[0, 0, 0],
       [rOH, 0, 0],
       [rOH * np.cos(x), 0 , rOH * np.sin(x)]]

atoms = Atoms('OH2', positions=pos)


# set N molecules with 2.75 angstrom separation

atoms.set_cell((2.75, 10+0.001 , 10)) # after repeat makes length 35

N = 10 # N-atoms chains

atoms = atoms.repeat((N, 1, 1))


# set cell

atoms.set_cell((40, 10+0.001 , 10))

atoms.center()


# set boundary conditions

atoms.set_pbc((True, False, False))
# atoms.set_pbc((False)) if we want no periodic boundary conditions

# view(atoms)




#-------------------------------------------------------------------------------------------------------------------
# First Equilibrate Using Cheap Method Tip4p Water Potential and Langevin Dynamics
# with Oxygens and Central Hydrogens fixed into the axis to distribute molecules 
# in random angles around central axis
#-------------------------------------------------------------------------------------------------------------------



# CONTRAINTS

# I contraint my core atoms to move in the x direction only

# Fixed Line Constraint

line = (1,0,0)

ind_not = [(3*i-1) for i in range(1,N+1)]
indices = [atom.index for atom in atoms if atom.index not in ind_not]

c = []

for i in indices:
    l = FixedLine(i,line)
    c.append(l)


# RATTLE-type bond lenghts constraints on O-H1, O-H2, H1-H2.

c2 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(N)
                                   for j in [0, 1, 2]])


    
c.append(c2)
    
# I add the constraints 

atoms.set_constraint(c)




# CALCULATOR

atoms.calc = TIP4P(rc=4.5)





# MOLECULAR DYNAMICS

# Langevin Dynamics

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
             friction=0.01, logfile=tag + '-langevin.log')

traj = Trajectory(tag + '-langevin.traj', 'w', atoms)
md.attach(traj.write, interval=1)
md.run(500)


#-------------------------------------------------------------------------------------------------------------------
# I Equilibrate Further Using Now Verlet Dynamics and Only Atoms Fixed into the Central Axis
#-------------------------------------------------------------------------------------------------------------------



# CONTRAINTS

# Fixed Atom
    
indices = [atom.index for atom in atoms if atom.symbol == 'O']

l = FixAtoms(indices)

c = []
c.append(l)

# I contraint my core atoms to move in the x direction only

# Fixed Line Constraint

line = (1,0,0)

ind_not = [(3*i-1) for i in range(1,N+1)]
indices = [atom.index for atom in atoms if atom.index not in ind_not]

c = []

for i in indices:
    l = FixedLine(i,line)
    c.append(l)

# RATTLE-type bond lenghts constraints on O-H1, O-H2, H1-H2.

c2 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(N)
                                   for j in [0, 1, 2]])


    
c.append(c2)
    
# I add the constraints 

atoms.set_constraint(c)




# CALCULATOR 

atoms.calc = TIP4P(rc=4.5)




# MOLECULAR DYNAMICS

# Velocity Verlet

dyn = VelocityVerlet(atoms, 5 * units.fs)
traj = Trajectory(tag + '-verlet-fix-H.traj', 'w', atoms)
dyn.attach(traj.write, interval=1)
dyn.run(1000)





#-------------------------------------------------------------------------------------------------------------------
# Equilibrate further using GPAW and Nanotube External Potential
#-------------------------------------------------------------------------------------------------------------------


# RATTLE-type constraints on O-H1, O-H2, H1-H2.

# atoms.constraints = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
#                                   for i in range(N)
#                                   for j in [0, 1, 2]])


# I contraint my core atoms to move in the x direction only


# Fixed Line Constraint

#line = (1,0,0)
#
ind_not = [(3*i-1) for i in range(1,N+1)]
indices = [atom.index for atom in atoms if atom.index not in ind_not]
#
#for i in indices:
#    c = FixedLine(i,line)
#    atoms.set_constraint(c)


# Fixed Atom
    
from ase.constraints import FixAtoms
c = FixAtoms(indices)
atoms.set_constraint(c)


# atoms.calc = TIP3P(rc=4.5)

atoms.calc = GPAW()

# Velocity Verlet

#dyn = VelocityVerlet(atoms, 5 * units.fs)
#traj = Trajectory(tag + '-verlet-tip3p.traj', 'w', atoms)
#dyn.attach(traj.write, interval=1)
#dyn.run(400)


# Langevin

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
             friction=0.01, logfile=tag + '-tip3p.log')

traj = Trajectory(tag + '-tip3p.traj', 'w', atoms)
md.attach(traj.write, interval=1)
md.run(1000)

#-------------------------------------------------------------------------------------------------------------------
# Equilibrate further using GPAW and Nanotube External Potential
#-------------------------------------------------------------------------------------------------------------------


## from ase.visualize import view
## view(atoms)
#
#
#r = 6 # diameter of the tube
#h = 200 # well depth
#
#class WellTubePotential(ExternalPotential):
#    def calculate_potential(self, gd):
#        r_vg = gd.get_grid_point_coordinates()
#        self.vext_g = [((r_vg[0]-atoms.get_cell()[0,0]/2)**2 + (r_vg[1]--atoms.get_cell()[1,1]/2)**2)**.5 < r/2]*h
#
#
## atoms.calc = GPAW(external=NanotubePotential(),gpts=(122, 122, 422))
## calc = GPAW(external=NanotubePotential(), gpts=(63, 63, 192), mode='lcao', symmetry={'point_group': False})
#
#atoms.set_constraint() # remove fixed bond lengths
#
#calc = GPAW(external=WellTubePotential(), gpts=gpts, mode=PW(350), symmetry={'point_group': False})
#
## calc = GPAW(external=NanotubePotential(), gpts=(32, 32, 96), mode='lcao')
#
#atoms.set_calculator(calc)
#
#
## Velocity Verlet Dynamics
#
##dyn = VelocityVerlet(atoms, 5 * units.fs)
##traj = Trajectory(tag + '.traj', 'w', atoms)
##dyn.attach(traj.write, interval=1)
##dyn.run(400)
#
## Langevin Dynamics
#
#md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
#              friction=0.01, logfile=tag + '.log')
#
#traj = Trajectory(tag + '.traj', 'w', atoms)
#md.attach(traj.write, interval=1)
#md.run(4000)

