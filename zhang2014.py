#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 11 12:42:31 2017

3 Donors and 1 Acceptor Model

@author: fcm
"""


from qutip import *
from pylab import *

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)

N=4 #I call N what in the paper is N + M
M=3

si = qeye(2); sx = sigmax(); sz = sigmaz(); sy = sigmay()
sp = sigmap(); sm = sigmam()


sx_list = []; sz_list = []; sp_list = []; sm_list = [];
for n in range(M):
    op_list = [si for m in range(M)]
    op_list[n] = sx
    sx_list.append(tensor(op_list))
    op_list[n] = sz
    sz_list.append(tensor(op_list))
    op_list[n] = sp
    sp_list.append(tensor(op_list))
    op_list[n] = sm
    sm_list.append(tensor(op_list))
    

# basis vectors
zero = basis(2,0)
up = zero
one = basis(2,1)
down = one

# construct the hamiltonian
h=1
w=1
J=1
sm=1

H = 0
# single spin
for n in range(M):
    H += h * w * sp_list[n] * sm_list[n]
# interaction terms
H += J * ( sm_list[0] * sp_list[1] + sm_list[1] * sp_list[2] + (sm_list[0] * sp_list[1]).dag() + (sm_list[1] * sp_list[2]).dag()) 

# hamitonian basis for N=3
# h00 element of matrix
e1 = tensor(zero, zero, zero)
h00 = e1.dag() * H * e1  
# etc
     

# eigenstates according to paper 
a1 = tensor(basis(2,0), basis(2,1), basis(2,1))
a2 = tensor(one, zero, one)
a3 = tensor(basis(2,1), basis(2,1), basis(2,0))

x1 = 0.5 * (a1 + sqrt(2) * a2 + a3) 
x2 = 1/sqrt(2) * (a1 - a3)
x3 = 0.5 * (a1 - sqrt(2) * a2 + a3) 

# obtain basis coefficients
a1.dag() * x1


        