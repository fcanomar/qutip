#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 20 19:50:04 2017

Two coupled Qubits

@author: fcm
"""


H = tensor(sigmaz(), Qobj(identity(2))) + tensor(Qobj(identity(2)), sigmaz()) + 0.05 * tensor(sigmax(), sigmax())
