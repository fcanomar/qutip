#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 27 12:34:57 2020

OHO wavefunctions

@author: fcm
"""

# creates: 2sigma.png, co_wavefunctions.png
import numpy as np
import matplotlib.pyplot as plt
from ase import Atoms
from ase.units import Bohr
from ase.io.cube import read_cube_data

from gpaw import GPAW
from gpaw.spherical_harmonics import Y

a = 8.0
L = 2 * a # O1 as zero

# s22 dimer positions
co = Atoms('OHHOHH', 
           positions=[(4., 3. , 3.),
           (3.70889836, 3.50935136, 2.241439  ),
           (3.70889836, 3.50935136, 3.758561  ),
           (6.91041909, 3.        , 3.        ),
           (5.94990784, 2.9191058 , 3.        ),
           (7.22441467, 2.09586604, 3.        )])

co.set_cell((2*a,a,a))
co.center()
co.calc = GPAW(mode='lcao',
               txt='CO.txt')
e = co.get_potential_energy()
L = 2 * co.get_positions()[0][0]
print(co.positions[:, 0] - L/2)

setups_oho = [co.calc.wfs.setups[1],co.calc.wfs.setups[3],co.calc.wfs.setups[5]]

plt.figure(figsize=(20, 10)) # set size

dpi = 100
colors = {0:'g',3:'r',4:'c'}
N = 100
for a, pp in enumerate(co.calc.wfs.setups):
    if a in [0,3,4]: # only OHO
        rc = max(pp.rcut_j)
        print(pp.rcut_j)
        x = np.linspace(-rc, rc, 2 * N + 1)
        P_i = co.calc.wfs.mykpts[0].projections[a][1] / Bohr**1.5
        phi_i = np.empty((len(P_i), len(x)))
        phit_i = np.empty((len(P_i), len(x)))
        i = 0
        for l, phi_g, phit_g in zip(pp.l_j, pp.data.phi_jg, pp.data.phit_jg):
            f = pp.rgd.spline(phi_g, rc + 0.3, l).map(x[N:]) * x[N:]**l
            ft = pp.rgd.spline(phit_g, rc + 0.3, l).map(x[N:]) * x[N:]**l
            for m in range(2 * l + 1):
                ll = l**2 + m
                phi_i[i, N:] = f * Y(ll, 1, 0, 0)
                phi_i[i, N::-1] = f * Y(ll, -1, 0, 0)
                phit_i[i, N:] = ft * Y(ll, 1, 0, 0)
                phit_i[i, N::-1] = ft * Y(ll, -1, 0, 0)
                i += 1
        x0 = co.positions[a, 0] - L / 2
        symbol = co.symbols[a]
        print(symbol, x0, rc)
        C = colors[a]
        plt.plot([x0], [0], color = 'gray', marker = 'o', ms=dpi * rc * 2 / 2.33 * 1.3 * Bohr, mfc='None', label='_nolegend_')
        plt.plot(x * Bohr + x0, P_i.dot(phit_i), C + '-', lw=1, label=r'$\tilde{\psi}^%s$' % symbol)
        plt.plot(x * Bohr + x0, P_i.dot(phi_i), C + '-', lw=2, label=r'$\psi^%s$' % symbol)

psit = co.calc.get_pseudo_wave_function(band=1)
n0 = psit.shape[0]
n1 = psit.shape[1]
psit2 = psit[:, :, n1 // 2] 
psit1 = psit2[:, n1 // 2]

x = np.linspace(-6.533, 9.4667, n0, endpoint=False)

plt.plot(x, psit1, 'bx', mew=2, label=r'$\tilde{\psi}$')

plt.legend(loc='best')
plt.xlabel('x [Å]')
plt.ylabel(r'$\psi$')
plt.ylim(ymin=-2, ymax=2)
# plt.show()
plt.savefig('oho_wavefunctions.png', dpi=dpi)

# Plot electron density

plt.figure(figsize=(20, 10))

denst = co.calc.get_all_electron_density()
dens2 = denst[:, :, n1 // 2] 
dens1 = dens2[:, n1 // 2]

x = np.linspace(-6.533, 9.4667, 2 * n0, endpoint=False) # finer grid

plt.plot(x, dens1, 'bx', mew=2, label=r'$\tilde{\psi}$')

plt.legend(loc='best')
plt.xlabel('x [Å]')
plt.ylabel(r'$\psi$')
plt.ylim(ymin=-2, ymax=2)

# Electron density map

dens_xy = dens[:, :,40]
plt.contourf(dens_xy)