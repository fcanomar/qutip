
# coding: utf-8

# # Two-Level System (TLS) Coupled to Harmonic Oscillators (HO)
# 
# Based on "The role of non-equilibrium vibrational structures in electronic coherence nd recoherence in pigment-protein complexes" by Chin, et al. (2013)
# 
# A Pigment-Protein Complex in modelled as a network of chromophoric sites, denoted |i>, each supporting a single optical excitation of energy e_i, which can transfer coherently onto another site j with a (dipolar) interaction amplitude J_ij. Each excitation is also linearly coupled to its local environment fluctuations, which are modelled as independent continua of harmonic vibrational modes.

# In[1056]:

from qutip import *
from pylab import *
from scipy import *
from math import factorial
import sympy

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)


# ## Two-Level Components (Pigment/Site) 

# Basis Vectors :

# In[1057]:

# |0> and |1> states 

zero = basis(2,0)
one = basis (2,1)

# vacuum
vac = Qobj([[0],[0]])


# In[1058]:

zero


# In[1059]:

one


# In[1060]:

vac


# #### System Two Two-Level Components/Sites

# Single Excitation Basis Vectors
# 
# |01> ie. |0> x |1>
# 
# |10> ie. |1> x |0>

# In[1061]:

e1 = tensor(zero, one)
e2 = tensor(one, zero)

e = [e1,e2]


# In[1062]:

e1


# In[1063]:

e2


# Hamiltonian of the System :

# In[1064]:

# parameters

eps = [10, 140]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])


# Hamiltonian

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()

HS


# In[1065]:

HS.eigenstates()


# I have obtained system's excitons

# In[1066]:

exc1 = HS.eigenstates()[1][0]
exc2 = HS.eigenstates()[1][3]
E1 = HS.eigenstates()[0][0]
E2 = HS.eigenstates()[0][3]


# They satisfy eigenvalues problem

# In[1067]:

HS * exc1 == E1 * exc1
HS * exc2 == E2 * exc2


# #### Two Two-Level System in Composite Basis (TLS + HO)

# In[1068]:

# Basis Vectors

e1 = tensor(zero, one, zero, zero)
e2 = tensor(one, zero, zero, zero)

e = [e1, e2]


# In[1069]:

e1


# In[1070]:

# System Hamiltonian

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()


# In[1071]:

HS.eigenstates()


# Excitons in |s1 s2 n1 n2> basis

# In[1072]:

Exc1 = HS.eigenstates()[1][0]
E1 = HS.eigenstates()[0][0]
Exc2 = HS.eigenstates()[1][15]
E2 = HS.eigenstates()[0][15]

Exc = [exc1, exc2]
E = [E1, E2]  


# Of course still satisfy eigenvalue problem

# In[1073]:

HS * Exc1 == E1 * Exc1
HS * Exc2 == E2 * Exc2


# They are just |exc> x |00>

# In[1074]:

Exc1 == tensor(exc1, zero, zero)
Exc2 == tensor(exc2, zero, zero)


# The energy gap is

# In[1075]:

E2 - E1


# ## Harmonic Oscillators (Bath)

# First I explore simple examples and then obtain Bath's Hamiltonian in composite basis

# #### Single Harmonic Oscillator

# In[1076]:

N = 2 # truncate at level 2

zero = basis(N,0) # |n> = |0>
one = basis(N,1) # |1>
vac = Qobj([[0],[0]])


# In[1077]:

zero


# In[1078]:

one


# #### System of Two Harmonic Oscillators

# In[1079]:

w1 = 1
w2 = 5

a1 = tensor(a,I)
a2 = tensor(I,a)

H2 = w1 * a1.dag() * a1 + w2 * a2.dag() * a2

H2


# In[1080]:

H2.eigenstates()


# This is

# In[1081]:

H2 * tensor(zero, zero) == tensor(vac, vac)   
H2 * tensor(one, zero) == w1 * tensor(one,zero)           
H2 * tensor(zero,one) == w2 * tensor(zero,one)
H2 * tensor(one, one)  == (w1 + w2) * tensor(one, one)


# #### Harmonic Oscillator in Composite Basis

# In[1082]:

a1 = tensor(I, I, a, I)
a2 = tensor(I, I, I, a)

e1 = tensor(zero, zero, zero, one) # |0001>
e2 = tensor(zero, zero, one, zero) # |0010> 

# Hamiltonian

HB = w1 * a1.dag() * a1 + w2 * a2.dag() * a2

# Eigenstates

HB * tensor(zero, zero, zero, zero) == tensor(vac, vac, vac, vac)  
HB * tensor(zero, zero, one, zero) == w1 * tensor(zero, zero, one,zero)
HB * tensor(zero, zero, zero, one) == w2 * tensor(zero, zero, zero, one)
HB * tensor(zero, zero, one, one) == (w1 + w2) * tensor(zero, zero, one, one)


# ## System and Bath Interaction

# System and Bath interaction is defined by the Spectral Density

# In[1083]:

ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22

def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ


# And mediated by the Mode Displacement Operator

# In[1084]:

X = sqrt(JJ(wa)) * (a1 + a1.dag()) + sqrt(JJ(wb)) * (a2 + a2.dag())


# The Interaction Hamiltonian is

# In[1085]:

HI = 0    
for i in range(N):
    HI += X * exc[i] * exc[i].dag()

HI


# In[1086]:

HI.eigenstates()


# In[ ]:




# In[1087]:

###############################################################


# ### Composite System (PPC)
# 
# I consider the system composed of 2 Two-Level Components and 2 Harmonic Oscillators. I choose the following basis:
# 
# |s1 s2 h1 h2> 
# 
# with s1, s2 = 0,1 ie. -1/2 and + 1/2
# and h1, h2 = 0,1 (energy levels)
# 
# this is
# 
# {|s1> x |s2> x |h1> x |h2>)

# In[ ]:




# In[1088]:

v1 = tensor(zero, zero, hzero, hzero)
v2 = tensor(zero, zero, hzero, hone)
v3 = tensor(zero, zero, hone, hzero)
v4 = tensor(zero, zero, hone, hone)
v5 = tensor(zero, one, hzero, hzero)
v6 = tensor(zero, one, hzero, hone)
v7 = tensor(zero, one, hone, hzero)
v8 = tensor(zero, one, hone, hone)
v9 = tensor(one, zero, hzero, hzero)
v10 = tensor(one, zero, hzero, hone)
v11 = tensor(one, zero, hone, hzero)
v12 = tensor(one, zero, hone, hone)
v13 = tensor(one, one, hzero, hzero)
v14 = tensor(one, one, hzero, hone)
v15 = tensor(one, one, hone, hzero)
v16 = tensor(one, one, hone, hone)


# In[1089]:

v1


# ### Operators in Composite Basis :

# Single Excitation States

# In[1090]:

e1 = tensor(one, zero, zero, zero)
e2 = tensor(zero, one, zero, zero)

e = [e1,e2]


# Creation and Annihilation Operators of the Bath 

# In[1091]:

I = qeye(2)
a = destroy(2)

a1 = tensor(I, I, a, I)
a2 = tensor(I, I, I, a)


# ## Hamiltonian of the System

# In[1092]:

# parameters

eps = [1, 131]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])


# ### Spectral Densities

# In[1093]:

# spectral densities
ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22

def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ


# ### Mode Displacement Operator

# In[1094]:

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )


# ### Hamiltonian

# In[1095]:

# System Hamiltonian

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()
    
# Interaction Hamiltonian

HI = 0    
for i in range(N):
    HI += X * e[i] * e[i].dag()
    
# Bath Hamiltonian

HB =   wa * a1.dag() * a1 + wb * a2.dag() * a2
                  
# Total Hamiltonian

H = HS + HI + HB

H


# In[1096]:

H.eigenstates()


# ### Excitons

# In[1097]:

exc1 = HS.eigenstates()[1][0]
E1 = HS.eigenstates()[0][0]
exc2 = HS.eigenstates()[1][15]
E2 = HS.eigenstates()[0][15]

exc = [exc1, exc2]
E = [E1, E2]  


# In[1098]:

exc1


# In[1099]:

exc2


# ### Hamiltonian in Exciton Basis

# In[1100]:

w = [wa, wb]

A = [a1, a2]

# |1> ie. |10> coefficients

c11 = exc1.dag() * e1
c12 = exc2.dag() * e1
              
# |2> ie. |01> coefficients
              
c21 = exc1.dag() * e2              
c22 = exc2.dag() * e2       
              
C = array([[c11, c12],[c21, c22]])              

# single excitation vectors in exciton basis              
              
e1 == c11 * exc1 + c12 * exc2
e2 == c21 * exc1 + c22 * exc2

# S and Q

S11 = sqrt(sqrt(JJ(wa)/wa))
S12 = sqrt(sqrt(JJ(wb)/wb))
S21 = S11
S22 = S12

S = array([[S11, S12], [S21, S22]])

def Q(n,m):
    Q = 0
    for i in range(N):
        for k in range(2):
            Q = sqrt(S[i,k]) * w[k] * C[i,n] * C[i,m] * (A[k] + A[k].dag())
    return Q

# HAMILTONIAN IN EXCITON BASIS

HSE = 0
for n in range(N):
    HSE += E[i] * exc[i] * exc[i].dag()
    for m in range(N):
        HSE += Q(n,m) * e[n] * e[m].dag()
    
HE = HSE + HB


# ## Time Evolution

# Initial State

# In[1101]:

psi0 = exc2

rho0 = ket2dm(psi0)


# In[1102]:

#partial trace
HEp = HE.ptrace([1,2])
rho0p = rho0.ptrace([1,2])

tlist = np.linspace(0, 2e-6, 100)

result = mesolve(HEp, rho0p, tlist, [], [])

result.states


# ### Reduced Density Matrix

# In[1103]:

rho0.ptrace([0,1])


# In[1104]:

l = len(result.states)

rdm = []
for i in range(l):
    s = result.states[i].ptrace([0,1])
    rdm.append(s)

rdm


# In[1105]:

#rdm = []
#for i in range(l):
#    s = result.states[i].ptrace([0,1])
#    rdm.append(s)

#rdm12 = []
#for i in range(l):
#    s = result.states[i][1,2]
#    rdm12.append(s)

