
from qutip import *
from pylab import *

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)


# ## State-Space and Vectors

# Case with N=3 donnor and M=1 acceptor. Each element has two possible states:
# 
# a) |up> which we represent in vector form (1,0)^t or |0> in abreviated form, this is the excited state
# 
# b) |down> which we represent in vector form (0,1)^t or |1> is the ground state
# 
# State-space of the composite system with 3 donnors and 1 acceptors:
# 
# Sabcd = Sa x Sb x Sc x Sd (tensor product)
# 
# Basis:
# 
# I can represent a state as
# 
# |psi> = sum over a, b, c, d of psi(a, b, c, d) |abcd>
# 
# The elements |abcd> represent my basis elements, so 2^4=16 basis vectors.
# 
# I construct these individual vectors and get their combined tensor representation:
# 

# |0> and |1> states 
zero = basis(2,0)
one = basis (2,1)

# We get the rest of tensors and store them in a list:

M=2
N=0

si = qeye(2)
sp = sigmap()
sm = sigmam()

sp_list = []; sm_list = [];
for n in range(M+N):
    op_list = [si for m in range(M+N)]
    op_list[n] = sp
    sp_list.append(tensor(op_list))
    op_list[n] = sm
    sm_list.append(tensor(op_list))


# ### We are ready to construct the Hamiltonian

epsa = 0.05
gamm = 0.05
J = 5
g = 0.05
kap = 0.05


#single particle
Hs = 0
for n in range(M+N):
    Hs += epsa * sp_list[n] * sm_list[n]


#different energy states
eps = [10,10]

#single particle
Hs = 0
for n in range(M+N):
    Hs += eps[n] * sp_list[n] * sm_list[n]  
    
    
#donor-acceptor
Hda= 0
for j in range(M):
    for c in range(M+1,M+N) :
        Hda =+ gamm * (sp_list[j] * sm_list[c] + sp_list[c] * sm_list[j])
        
#donor-donor
Hdd= 0
for j in range(M):
    for k in range(M):
        if k>j :     
            Hdd =+ J * (sp_list[j] * sm_list[k] + sp_list[k] * sm_list[j])        

#acceptor-acceptor
Haa= 0
for c in range(M+1, M+N):
    for r in range(M+1,M+N):
        if r > c :
            Haa =+ gamm * (sp_list[c] * sm_list[r] + sp_list[r] * sm_list[c])
            

H = Hs + Hda + Hdd + Haa
H


# Let's calculate the Eigenstates of the Hamiltonian

H.eigenstates()

# time evolution

# state vector evolution M = 2 N = 0

#psi0 = tensor(zero, one)
#psi0 = 1/sqrt(2)*(tensor(zero, one) + tensor(one, zero))
#
#
#times = linspace(0.0, 200.0, 500.0)
#l = len(times)
#
#result = mesolve(H, psi0, times, [], [])
#
#x0 = []
#x1 = []
#x2 = []
#x3 = []
#for i in range(l):
#    psi = result.states[i]
#    s0 = float(psi[0].real)
#    x0.append(s0)
#    s1 = float(psi[1].real)
#    x1.append(s1)
#    s2 = float(psi[2].real)
#    x2.append(s2)
#    s3 = float(psi[3].real)
#    x3.append(s3)
#
#plt.plot(times, x0)
#plt.plot(times, x1)
#plt.plot(times, x2)
#plt.plot(times, x3)
#plt.ylabel('')
#plt.xlabel('Time')
##savefig('name.png')
#show() 

# density matrix evolution

# base vectors

e_list = []; op_list = []
for n in range(M):
    op_list = [one for m in range(M)]
    op_list[n] = zero
    e_list.append(tensor(op_list))

# contains base vectors for donors and acceptors    
et_list = []; op_list = []
for n in range(M+N):
    op_list = [one for m in range(M+N)]
    op_list[n] = zero
    et_list.append(tensor(op_list))


# time evolution

times = linspace(0.0, 200.0, 500.0)
l = len(times)

m = 4

psid = 0
for i in range(m):
    psid += 1/sqrt(m) * e_list[i]

a_list = [one for m in range(N)]
psia = tensor(a_list)

psit = tensor(psid, psia)

rhot = ket2dm(psit)

result = mesolve(H, rhot, times, [], [])

# time evolution two donnors

times = linspace(0.0, 10.0, 500.0)
l = len(times)

psi0 = 1/sqrt(2)* (tensor(one, zero) + tensor(zero, one))

result = mesolve(H, psi0, times, [], [])

x = []
for i in range(l):
    psi = result.states[i]
    s = round(psi[1].real,5)
    x.append(s)
    
plt.plot(times, x)
plt.ylabel('')
plt.xlabel('Time')
show()  

# efficiency

# function for single excitation density matrix
# ! no considerada single-excitation-conditioned density matrix

def wd(rho):
    wd = 0
    for j in range(M):
        wd += 2 * gamm * et_list[j].dag() * rho * et_list[j]
    return wd

def wrc(rho):
    wrc = 0
    for c in range(M+1, M+N, 1):
        wrc += 2 * kap * et_list[c].dag() * rho * et_list[c]
    return wrc

# get efficiency temporal lists

wd_list = []
for i in range(l):
    wd_list.append(wd(result.states[i]))
 
wrc_list = []
for i in range(l):
    wrc_list.append(wrc(result.states[i]))
    
# time evolution

x = []
for i in range(l):
    rdm = result.states[i]
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho01_rdm_triplet.png')
show()  
