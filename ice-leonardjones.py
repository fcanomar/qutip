#!/usr/bin/env python2
# -*- coding: utf-8 -*-

"""
Created on Sun Jan  6 12:19:45 2019

Ice Sheet in LJ Nanotube Potential

@author: fcm

"""

from ase.units import Bohr
from gpaw import GPAW, PW
from gpaw.external import ExternalPotential
from ase import Atoms, Atom
import numpy as np
from ase.calculators.tip4p import TIP4P, rOH, angleHOH
from math import pi, cos, sin
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.constraints import FixBondLengths, FixAtoms
from ase.md import Langevin

from ase.visualize import view

#-------------------------------------------------------------------------------------------------------------------
# I set the ice sheet and the internal water chain
#-------------------------------------------------------------------------------------------------------------------

# I add a central water molecule

x = angleHOH * np.pi / 180 / 2

pos = [[0, 0, 0],
       [rOH, 0, 0],
       [rOH * np.cos(x), 0 , rOH * np.sin(x)]]
       

atoms = Atoms('OH2', positions=pos)

# I add the rest of the molecules forming an octogon

d = 2.8
n = 8 
theta = 2 * pi / n
dist = d/2/sin(theta/2)

for i in range(1, n+1):
        x0 = dist * cos(i*theta)
        y0 = dist * sin(i*theta)
        pos = [[x0, y0, 0],
               [x0, y0, rOH],
               [x0 + rOH * np.sin(x), y0 , rOH * np.cos(x)]]
        atoms.append(Atom('O', pos[0]))
        atoms.append(Atom('H', pos[1]))
        atoms.append(Atom('H', pos[2]))
        # atoms[:-3].rotate(theta,'z')

# I set the cell for the unit
       
a = 3

atoms.set_cell((5*a+0.001, 5*a, 2.8)) 
atoms.center()

# I repeat along the z axis N times - separation 2.8 angstroms

N = 3

atoms = atoms.repeat((1, 1, N))

# Set cell for the whole

atoms.set_cell((5*a+0.001 , 5*a, 2.8*N))

atoms.center()

# Set boundary conditions

atoms.set_pbc((False, False, True))


#-------------------------------------------------------------------------------------------------------------------
# I constrain the Oxygen atoms and use Langevin dynamics to induce disorder in the protons
#-------------------------------------------------------------------------------------------------------------------

# Fixed Oxygens
    
indices = [atom.index for atom in atoms if atom.symbol == 'O']

l = FixAtoms(indices)

c = []
c.append(l)

# Rattle-type bond lenghts constraints on O-H1 O-H2 and H1-H2

Nt = 9 * N
c2 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(Nt)
                                   for j in [0, 1, 2]]) 
c.append(c2)

# I add both type of constraints

atoms.set_constraint(c)

# I set the calculator

atoms.calc = TIP4P(rc=3.5)

# I use Langevin dynamics to add disorder to the H positions

tag='ice-leonardjones'

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
              friction=0.01, logfile=tag + '-langevin.log') # no log file, add logfile=tag + '-langevin.log' to export logfile

traj = Trajectory(tag + '-langevin.traj', 'w', atoms)

md.attach(traj.write, interval=1)

md.run(50)

#-------------------------------------------------------------------------------------------------------------------
# I define a Leonard Jones potential to model the interaction with the Nanotube surface
#-------------------------------------------------------------------------------------------------------------------     

eps = 0.005 # depth of the well
sigma = 3.8 # distance for V=0
D = 14 # nanotube diameter
class LJ_Potential(ExternalPotential):
    def calculate_potential(self, gd):
        r_vg = gd.get_grid_point_coordinates()
        r = ((r_vg[1] - a / Bohr / 2)**2 + (r_vg[2] - a / Bohr / 2)**2)**.5
        self.vext_g = 4 * eps * ((sigma/(D-r))^12-(sigma/(D-r))^6)  

    def todict(self):
        return {}

#-------------------------------------------------------------------------------------------------------------------
# I remove constraints and do GPAW MD
#-------------------------------------------------------------------------------------------------------------------

atoms.set_constraint([])

calc = GPAW(external=LJ_Potential(), mode=PW(100))
atoms.calc = calc

md = Langevin(atoms, 1 * units.fs, temperature=7 * units.kB,
              friction=0.01, logfile=tag + '.log')

traj = Trajectory(tag + '.traj', 'w', atoms)
md.attach(traj.write, interval=1)
md.run(4000)

