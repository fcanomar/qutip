#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun Dec 16 18:57:49 2018

Square-ice sheet wrapped into a cylinder inside the carbon nanotube and interior molecules in a chainlike configuration (Kolesnikov, 2004)

Spectra and Proton Momentum Distribution (Including Sigma as a Measure for Kinetic Energy)

@author: fcm
"""


from ase import Atoms, Atom
import numpy as np
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.visualize import view
from math import pi, cos, sin
from ase.visualize import view
from ase import Atoms
from ase.md import Langevin, VelocityVerlet
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.constraints import FixBondLengths, FixedLine, FixAtoms
from ase.build import nanotube
import numpy as np
from gpaw.external import ExternalPotential
from gpaw import GPAW, PW
# import plot_potential_xy as plxy
import matplotlib.pyplot as plt
from pylab import savefig
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.calculators.tip4p import TIP4P
import plot_potential_xy as plxy

tag = 'water-ice-and-chain'
gpts= (64, 64, 164)

#-------------------------------------------------------------------------------------------------------------------
# I set the nanotube
#-------------------------------------------------------------------------------------------------------------------

ntb = nanotube(14, 0, length=10)
ntb.set_cell((20+0.001 , 20, 60))
ntb.center()

ntb.center()
ntb.calc = GPAW(gpts=gpts)
# ntb.calc = GPAW(gpts=(20, 20, 55))

#  ------> at least run and stop to get next line going
ntb.get_potential_energy()

potential = ntb.calc.get_electrostatic_potential()

plxy.plot_potential_xy(ntb, tag='60-')



ntb.calc.attach(ntb.calc.write, 'nanotube-potential.gpw')



view(ntb)

#-------------------------------------------------------------------------------------------------------------------
# I set the ice sheet and the internal water chain
#-------------------------------------------------------------------------------------------------------------------


# I add a central water molecule

x = angleHOH * np.pi / 180 / 2

pos = [[0, 0, 0],
       [rOH, 0, 0],
       [rOH * np.cos(x), 0 , rOH * np.sin(x)]]
       

atoms = Atoms('OH2', positions=pos)

# I add the rest of the molecules forming an octogon

d = 2.8
n = 8 
theta = 2 * pi / n
dist = d/2/sin(theta/2)

for i in range(1, n+1):
        x0 = dist * cos(i*theta)
        y0 = dist * sin(i*theta)
        pos = [[x0, y0, 0],
               [x0+rOH, y0, 0],
               [x0+rOH * np.cos(x), y0 , rOH * np.sin(x)]]
        atoms.append(Atom('O', pos[0]))
        atoms.append(Atom('H', pos[1]))
        atoms.append(Atom('H', pos[2]))


# I repeat along the z axis 

# set N molecules with 2.75 angstrom separation

atoms.set_cell((10+0.001, 10, 2.75)) # after repeat makes length 35

N = 13 # N-atoms chains

atoms = atoms.repeat((1, 1, N))


# set cell

atoms.set_cell((20+0.001 , 20, 60))

atoms.center()


# set boundary conditions

atoms.set_pbc((False, False, False))
# atoms.set_pbc((False)) if we want no periodic boundary conditions

ntb.extend(atoms)
view(ntb)

# view(atoms)

#-------------------------------------------------------------------------------------------------------------------
# Tip4p Water Potential 
# Langevin Dynamics
# Oxygens fixed
#-------------------------------------------------------------------------------------------------------------------


# CONTRAINTS

# Fixed Atom
    
indices = [atom.index for atom in atoms if atom.symbol == 'O']

l = FixAtoms(indices)

c = []
c.append(l)

# RATTLE-type bond lenghts constraints on O-H1, O-H2, H1-H2.

Nt = 9 * N
c2 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(Nt)
                                   for j in [0, 1, 2]])
   
c.append(c2)
    
# I add the constraints 

atoms.set_constraint(c)



# CALCULATOR

atoms.calc = TIP4P(rc=4.5)



# MOLECULAR DYNAMICS

#dyn = VelocityVerlet(atoms, 5 * units.fs)
#traj = Trajectory(tag + '-verlet.traj', 'w', atoms)
#dyn.attach(traj.write, interval=1)
#dyn.run(300)


# Langevin Dynamics

# md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
#             friction=0.01, logfile=tag + '-langevin.log')

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
             friction=0.01)

traj = Trajectory(tag + '-langevin.traj', 'w', atoms)


def printenergy(a=atoms):  # store a reference to atoms in the definition.
    """Function to print the proton potential, kinetic and total energy."""
    epot = a.get_potential_energy() / len(a)
    ekin = a.get_kinetic_energy() / len(a)
    print('Energy per atom: Epot = %.3feV  Ekin = %.3feV (T=%3.0fK)  '
          'Etot = %.3feV' % (epot, ekin, ekin / (1.5 * units.kB), epot + ekin))



md.attach(printenergy, interval=1)

md.attach(traj.write, interval=1)

md.run(150)

# I equilibrate further using MD

dyn = VelocityVerlet(atoms, 5 * units.fs)
traj = Trajectory(tag + '-verlet.traj', 'w', atoms)
dyn.attach(traj.write, interval=10)

md.run(300)
