#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May  1 10:16:23 2018

@author: fcm
"""

from ase.visualize import view
from ase import Atoms
from ase.units import Hartree, Bohr
from ase.constraints import FixBondLengths
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.optimize import QuasiNewton
from ase.md import Langevin
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.build import nanotube
import numpy as np
from gpaw.external import ExternalPotential
from gpaw import GPAW
import plot_potential_xy as plxy


##########################################################################################
# FIRST I CREATE AND CALCULATE POTENTIAL FROM THE NANOTUBE
##########################################################################################

# source plot_potential_xy.py first

ntb = nanotube(6, 0, length=20)
ntb.set_cell((10, 10+0.001 , 27.5))
ntb.center()
ntb.calc = GPAW(gpts=(40, 40, 110))
# ntb.calc = GPAW(gpts=(20, 20, 55))

#  ------> at least run and stop to get next line going
ntb.get_potential_energy()

potential = ntb.calc.get_electrostatic_potential()

plxy.plot_potential_xy(ntb, tag='60-')



ntb.calc.attach(ntb.calc.write, 'nanotube-potential.gpw')


view(ntb)


##########################################################################################
# I DO MD OF WATER MOLECULES USING GPAW IN THE NANOTUBE POTENTIAL
##########################################################################################


# Set up water box at 20 deg C density

# ntb, ntb.calc = restart('xyz.gpw')

x = angleHOH * np.pi / 180 / 2
pos = [[0, 0, 0],
       [0, rOH * np.cos(x), rOH * np.sin(x)],
       [0, rOH * np.cos(x), -rOH * np.sin(x)]]
atoms = Atoms('OH2', positions=pos)

# atoms.set_cell((10, 10 + 0.0001, 5)) # after repeat makes length 35
atoms.set_cell((10, 10+0.001 , 2.75)) # after repeat makes length 35

# atoms.center()

N =10 # N-atoms chains
atoms = atoms.repeat((1, 1, N))

# atoms.set_pbc(False)

atoms.set_cell((10, 10+0.001 , 27.5))
atoms.center()

atoms.set_pbc((False, False, True))


# break simmetry

####### ---->

# RATTLE-type constraints on O-H1, O-H2, H1-H2.

#atoms.constraints = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
#                                   for i in range(N)
#                                   for j in [0, 1, 2]])

####### <----


# First Equilibrate Using Cheap Method Tip3p Water Potential

#tag = 'water-chain-from-box-md-tip3p-nanotube-potential'
#
#atoms.calc = TIP3P(rc=4.5)
#md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
#              friction=0.01, logfile=tag + '.log')
#
#traj = Trajectory(tag + '.traj', 'w', atoms)
#md.attach(traj.write, interval=1)
#md.run(4000)


# Equilibrate further using GPAW and Nanotube External Potential

# from ase.visualize import view
# view(atoms)

from gpaw.external import ConstantPotential

class NanotubePotential(ExternalPotential):
    def calculate_potential(self, gd):
        self.vext_g = potential[0:79,0:79,0:220] # drop one element in each dimension to fit given potential 


tag = 'water-chain-from-box-md-gpaw-nanotube-potential-ten-pbc'
# atoms.calc = GPAW(external=NanotubePotential(),gpts=(122, 122, 422))
# calc = GPAW(external=NanotubePotential(), gpts=(63, 63, 192), mode='lcao', symmetry={'point_group': False})

calc = GPAW(external=NanotubePotential(), gpts=(40,40,110), mode='lcao', symmetry={'point_group': False})

# calc = GPAW(external=NanotubePotential(), gpts=(32, 32, 96), mode='lcao')

atoms.set_calculator(calc)

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
              friction=0.01, logfile=tag + '.log')

traj = Trajectory(tag + '.traj', 'w', atoms)
md.attach(traj.write, interval=1)
md.run(4000)

# Preoteasa's Questions

from numpy import linalg as la

traj = Trajectory('water-chain-from-box-md-gpaw-nanotube-potential.traj')

temp = []

for atoms in traj:
    temp.append(atoms.get_temperature())

plt.plot(temp)
   
angm = []

for atoms in traj:
    angm.append(la.norm(atoms.get_angular_momentum()))

plt.plot(angm)    

mom = []

for atoms in traj:
    mom.append(la.norm(atoms.get_moments_of_inertia()))

plt.plot(mom)   


# plot 3d
    
from mpl_toolkits.mplot3d import axes3d

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

X, Y, Z = cm1[1:10]
ax.plot_wireframe(X,Y,Z)

plt.show()


####################################################################################

#####       VISUALIZATION

####################################################################################

from numpy import linalg as la
import matplotlib.pyplot as plt

# distances between molecules

tr_cm1 = []
tr_cm2 = []
tr_cm3 = []

tr_dist12 = []
tr_dist13 = []
tr_dist23 = []

for atoms in traj:
    cm1 = atoms[0:3].get_center_of_mass()
    cm2 = atoms[3:6].get_center_of_mass()
    cm3 = atoms[6:10].get_center_of_mass()
    
    tr_cm1.append(cm1)
    tr_cm2.append(cm2)
    tr_cm3.append(cm3)
    
    dist12 = la.norm(cm1 - cm2)
    dist13 = la.norm(cm1 - cm3)
    dist23 = la.norm(cm2 - cm3)
    
    tr_dist12.append(dist12)
    tr_dist13.append(dist13)
    tr_dist23.append(dist23)
    

# Plot distance between molecules

plt.plot(tr_dist12)
plt.plot(tr_dist23)
plt.plot(tr_dist13)


