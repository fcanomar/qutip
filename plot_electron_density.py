#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 16:14:34 2019

Plot electron density on a plane

@author: fcm
"""


import numpy as np
import matplotlib.pyplot as plt
from pylab import savefig

def plot_electron_density_xy(sys, vfrom=None, vto=None , tag=None):
    
    ax = sys.cell[0,0]
    ay = sys.cell[1,1]
    
    #n = calc.get_all_electron_density(gridrefinement=2)
    
    nx, ny, nz = sys.calc.get_all_electron_density(gridrefinement=2).shape
    
    x = np.linspace(0, ax, nx, endpoint=False)
    y = np.linspace(0, ay, ny, endpoint=False)
    
    v = sys.calc.get_all_electron_density(gridrefinement=2)
    
    v_xy = v[slice(0,nx),slice(0,ny),nz//2] # Plane XY

    plt.figure(figsize=(13, 10))

    if vfrom == None: 
        vfrom = v_xy.min()
        
    if vto == None:
        vto = v_xy.max()
        
    colorbar = np.linspace(vfrom, vto, 15)
    
    plt.contourf(x, y, v_xy.T, colorbar)
    plt.colorbar()
    plt.title('Electron Density [' + tag + ']')

    if tag != None:
        savefig(tag + '-electron-density.png')