#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 29 23:02:13 2019

Electronic Periodic State

@author: fcm
"""

from math import pi, sqrt

from ase.visualize import view
from ase import Atoms
from ase.md import Langevin, VelocityVerlet
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.constraints import FixBondLengths, FixedLine, FixAtoms
from ase.build import nanotube
import numpy as np
from gpaw.external import ExternalPotential
from gpaw import GPAW, PW
# import plot_potential_xy as plxy
import matplotlib.pyplot as plt
from pylab import savefig
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.calculators.tip4p import TIP4P
from gpaw.transformers import Transformer
from gpaw.lfc import LFC, BasisFunctions
from gpaw.utilities import (unpack2, unpack_atomic_matrices,
                            pack_atomic_matrices)



####################################################################################################################
#
# 0. Initial Settings
#
####################################################################################################################

# ntb, ntb.calc = restart('xyz.gpw')

tag = 'water-chain-periodic'

gpts = (16*3, 16*3, 16*6)

#-------------------------------------------------------------------------------------------------------------------
# I set the chain of water atoms and the cell
#-------------------------------------------------------------------------------------------------------------------

# water molecule

x = angleHOH * np.pi / 180 / 2
pos = [[0, 0, 0],
       [rOH, 0, 0],
       [rOH * np.cos(x), 0 , rOH * np.sin(x)]]

atoms = Atoms('OH2', positions=pos)


# set N molecules with 2.75 angstrom separation

atoms.set_cell((2.75, 10+0.001 , 10)) # after repeat makes length 35

N = 10 # N-atoms chains

atoms = atoms.repeat((N, 1, 1))


# set cell

atoms.set_cell((40, 10+0.001 , 10))

atoms.center()


# set boundary conditions

atoms.set_pbc((False, False, False))
# atoms.set_pbc((False)) if we want no periodic boundary conditions

# view(atoms)


#-------------------------------------------------------------------------------------------------------------------
# I fix the atoms and calculate the ground state
#-------------------------------------------------------------------------------------------------------------------


# CONTRAINTS

# Fixed Atoms H and O
    
indices = [atom.index for atom in atoms]

l = FixAtoms(indices)

c = []
c.append(l)

# I add the constraints 

atoms.set_constraint(c)



####################################################################################################################
#
# A. GPAW 3D GRID
#
####################################################################################################################


#-------------------------------------------------------------------------------------------------------------------
# I do Groundstate Calculation using LCAO Basis
#-------------------------------------------------------------------------------------------------------------------

# CALCULATOR 

calc = GPAW()

atoms.set_calculator(calc)

atoms.get_potential_energy()


#-------------------------------------------------------------------------------------------------------------------
# I obtain and plot Wavefunction
#-------------------------------------------------------------------------------------------------------------------

wf = atoms.calc.get_pseudo-wave_function()

#-------------------------------------------------------------------------------------------------------------------
# I obtain and plot AE Electron Density 
#-------------------------------------------------------------------------------------------------------------------

n = atoms.calc.get_all_electron_density()



####################################################################################################################
#
# B. LCAO BASIS
#
####################################################################################################################


#-------------------------------------------------------------------------------------------------------------------
# I do Groundstate Calculation using LCAO Basis
#-------------------------------------------------------------------------------------------------------------------

# CALCULATOR 

calc = GPAW(mode='lcao', basis='dzp')

atoms.set_calculator(calc)

atoms.get_potential_energy()


#-------------------------------------------------------------------------------------------------------------------
# I obtain and plot Wavefunction
#-------------------------------------------------------------------------------------------------------------------

wf = atoms.calc.get_pseudo-wave_function()

#-------------------------------------------------------------------------------------------------------------------
# I obtain and plot AE Electron Density 
#-------------------------------------------------------------------------------------------------------------------


#-------------------------------------------------------------------------------------------------------------------
# I impose Time Period
#-------------------------------------------------------------------------------------------------------------------


nt = calc.get_electron_density()

n  = atoms.calc.get_all_electron_density()

# wf = atoms.calc.get_wave_function()

N = 10

psi = []

for n in range(0,N):
    psi.append(np.exp(-1j*2*np.pi*n/N))

wf_psi =[]

for n in range(0,N):
    wf_psi.append(wf*psi[n])


##-------------------------------------------------------------------------------------------------------------------
## I obtain all-electron Wavefunction
##-------------------------------------------------------------------------------------------------------------------
#
#
#import matplotlib.pyplot as plt
#from ase import Atoms
#from ase.units import Bohr
#from gpaw.utilities.ps2ae import PS2AE
#from gpaw import GPAW
#
#hli = Atoms('HLi', positions=[[0, 0, 0], [0, 0, 1.6]])
#hli.center(vacuum=2.5)
#hli.calc = GPAW(txt='hli.txt', mode='fd')
#hli.get_potential_energy()
#
## Transformer:
#t = PS2AE(hli.calc, h=0.05)
#
#for n, color in enumerate(['green', 'red']):
#    ps = t.get_wave_function(n, ae=False)
#    ae = t.get_wave_function(n)
#    norm = t.gd.integrate(ae**2) * Bohr**3
#    print('Norm:', norm)
#    assert abs(norm - 1) < 1e-2
#    i = ps.shape[0] // 2
#    x = t.gd.coords(2) * Bohr
#
#    # Interpolated PS and AE wfs:
#    plt.plot(x, ps[i, i], '--', color=color,
#             label=r'$\tilde\psi_{}$'.format(n))
#    plt.plot(x, ae[i, i], '-', color=color,
#             label=r'$\psi_{}$'.format(n))
#
#    # Raw PS wfs:
#    ps0 = hli.calc.get_pseudo_wave_function(n, pad=True)
#    gd = hli.calc.wfs.gd
#    i = ps0.shape[0] // 2
#    X = gd.coords(2) * Bohr
#    plt.plot(X, ps0[i, i], 'o', color=color)
#
#plt.plot(x, 0 * x, 'k')
#plt.xlabel('z [Ang]')
#plt.ylabel('wave functions [Ang$^{-3/2}$]')
#plt.legend()
#plt.savefig('hli-wfs.png')
#hli.calc.write('hli.gpw')
#
#
##-------------------------------------------------------------------------------------------------------------------
## I modify the get_all_electron_density function to calculate the electron density for each time-step based on the
## evolved     
##-------------------------------------------------------------------------------------------------------------------
#
#
#def get_all_electron_density(self, atoms=None, gridrefinement=2,
#                             spos_ac=None, skip_core=False):
#    """Return real all-electron density array.
#
#       Usage: Either get_all_electron_density(atoms) or
#                     get_all_electron_density(spos_ac=spos_ac)
#
#       skip_core=True theoretically returns the
#                      all-electron valence density (use with
#                      care; will not in general integrate
#                      to valence)
#    """
#    if spos_ac is None:
#        spos_ac = atoms.get_scaled_positions() % 1.0
#
#    # Refinement of coarse grid, for representation of the AE-density
#    # XXXXXXXXXXXX think about distribution depending on gridrefinement!
#    if gridrefinement == 1:
#        gd = self.redistributor.aux_gd
#        n_sg = self.nt_sG.copy()
#        # This will get the density with the same distribution
#        # as finegd:
#        n_sg = self.redistributor.distribute(n_sg)
#    elif gridrefinement == 2:
#        gd = self.finegd
#        if self.nt_sg is None:
#            self.interpolate_pseudo_density()
#        n_sg = self.nt_sg.copy()
#    elif gridrefinement == 4:
#        # Extra fine grid
#        gd = self.finegd.refine()
#
#        # Interpolation function for the density:
#        interpolator = Transformer(self.finegd, gd, 3)  # XXX grids!
#
#        # Transfer the pseudo-density to the fine grid:
#        n_sg = gd.empty(self.nspins)
#        if self.nt_sg is None:
#            self.interpolate_pseudo_density()
#        for s in range(self.nspins):
#            interpolator.apply(self.nt_sg[s], n_sg[s])
#    else:
#        raise NotImplementedError
#
#    # Add corrections to pseudo-density to get the AE-density
#    splines = {}
#    phi_aj = []
#    phit_aj = []
#    nc_a = []
#    nct_a = []
#    for a, id in enumerate(self.setups.id_a):
#        if id in splines:
#            phi_j, phit_j, nc, nct = splines[id]
#        else:
#            # Load splines:
#            phi_j, phit_j, nc, nct = self.setups[a].get_partial_waves()[:4]
#            splines[id] = (phi_j, phit_j, nc, nct)
#        phi_aj.append(phi_j)
#        phit_aj.append(phit_j)
#        nc_a.append([nc])
#        nct_a.append([nct])
#
#    # Create localized functions from splines
#    phi = BasisFunctions(gd, phi_aj)
#    phit = BasisFunctions(gd, phit_aj)
#    nc = LFC(gd, nc_a)
#    nct = LFC(gd, nct_a)
#    phi.set_positions(spos_ac)
#    phit.set_positions(spos_ac)
#    nc.set_positions(spos_ac)
#    nct.set_positions(spos_ac)
#
#    I_sa = np.zeros((self.nspins, len(spos_ac)))
#    a_W = np.empty(len(phi.M_W), np.intc)
#    W = 0
#    for a in phi.atom_indices:
#        nw = len(phi.sphere_a[a].M_w)
#        a_W[W:W + nw] = a
#        W += nw
#
#    x_W = phi.create_displacement_arrays()[0]
#    D_asp = self.D_asp  # XXX really?
#
#    rho_MM = np.zeros((phi.Mmax, phi.Mmax))
#    for s, I_a in enumerate(I_sa):
#        M1 = 0
#        for a, setup in enumerate(self.setups):
#            ni = setup.ni
#            D_sp = D_asp.get(a)
#            if D_sp is None:
#                D_sp = np.empty((self.nspins, ni * (ni + 1) // 2))
#            else:
#                I_a[a] = ((setup.Nct) / self.nspins -
#                          sqrt(4 * pi) *
#                          np.dot(D_sp[s], setup.Delta_pL[:, 0]))
#
#                if not skip_core:
#                    I_a[a] -= setup.Nc / self.nspins
#
#            if gd.comm.size > 1:
#                gd.comm.broadcast(D_sp, D_asp.partition.rank_a[a])
#            M2 = M1 + ni
#            rho_MM[M1:M2, M1:M2] = unpack2(D_sp[s])
#            M1 = M2
#
#        assert np.all(n_sg[s].shape == phi.gd.n_c)
#        phi.lfc.ae_valence_density_correction(rho_MM, n_sg[s], a_W, I_a,
#                                              x_W)
#        phit.lfc.ae_valence_density_correction(-rho_MM, n_sg[s], a_W, I_a,
#                                               x_W)
#
#    a_W = np.empty(len(nc.M_W), np.intc)
#    W = 0
#    for a in nc.atom_indices:
#        nw = len(nc.sphere_a[a].M_w)
#        a_W[W:W + nw] = a
#        W += nw
#    scale = 1.0 / self.nspins
#
#    for s, I_a in enumerate(I_sa):
#
#        if not skip_core:
#            nc.lfc.ae_core_density_correction(scale, n_sg[s], a_W, I_a)
#
#        nct.lfc.ae_core_density_correction(-scale, n_sg[s], a_W, I_a)
#        gd.comm.sum(I_a)
#        N_c = gd.N_c
#        g_ac = np.around(N_c * spos_ac).astype(int) % N_c - gd.beg_c
#
#        if not skip_core:
#
#            for I, g_c in zip(I_a, g_ac):
#                if (g_c >= 0).all() and (g_c < gd.n_c).all():
#                    n_sg[s][tuple(g_c)] -= I / gd.dv
#
#    return n_sg, gd


#-------------------------------------------------------------------------------------------------------------------
# I obtain averaged Potential acting on the Protons
#-------------------------------------------------------------------------------------------------------------------
    
    
#-------------------------------------------------------------------------------------------------------------------
# I do Groundstate Calculation using LCAO basis
#-------------------------------------------------------------------------------------------------------------------


# CALCULATOR 

calc = GPAW(nbands=40,
            h=0.2,
            mode='lcao',
            basis='dzp')

# try with sto-3G for simpler treatment

atoms.set_calculator(calc)

atoms.get_potential_energy()

    