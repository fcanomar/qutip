#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 17:31:10 2017

Nature Dimer Coupled to Bath - Pauli Matrices

@author: fcm
"""

from qutip import *
from pylab import *
from scipy import *
from math import factorial
import sympy
import os

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)

# working directory

os.chdir('/Users/fcm/qutip')

#dimer

N=2

# state space

# |0> and |1> states 
zero = basis(2,0)
one = basis (2,1)

e1 = tensor(one, zero, zero, zero)
e2 = tensor(zero, one, zero, zero)

e = [e1,e2]

# parameters

eps = [140, 140]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])

# spectral densities

ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22

def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ



# mode displacement operator

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )

#################################################################################

# spin boson simplified
E = 10
V= 10

H = E * tensor(sigmaz(), qeye(2)) + V * tensor((a.dag() + a), sigmax())

# sigmaz

times = linspace(0.0, 100.0, 600.0)
sing = 1/sqrt(2) * (zero + one)
psi0 = tensor(sing,zero)
rho0 = ket2dm(psi0)
result = mesolve(H, rho0, times, [], [tensor(sigmaz(),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
#savefig('sigmaz.png')
show()

# coherence

times = linspace(0.0, 8.0, 100.0)
sing = 1/sqrt(2) * (zero + one)
psi0 = tensor(sing,zero)
rho0 = ket2dm(psi0)
result = mesolve(H, rho0, times, [], [])

l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('Rho_00')
plt.xlabel('Time')
#savefig('sigmaz.png')
show()

# reduced density matrix

times = linspace(0.0, 4.0, 600.0)
sing = 1/sqrt(2) * (zero + one)
rho0 = ket2dm(sing)
# H = H.ptrace(0)
result = mesolve(H, rho0, times, [], [sigmaz()])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
#savefig('sigmaz.png')
show()


#################################################################################

# HAMILTONIAN

zero = basis(2,0)
one = basis(2,1)

e1 = tensor(zero, one)
e2 = tensor(one, zero)

eps1 = eps[0]
eps2 = eps[1]
J12 = J[0,1]

# System's hamiltonian in |00> |01> ... basis

HS = eps1 * e1 * e1.dag() + eps2 * e2 * e2.dag() + J12 * (e1 * e2.dag() + e2 * e1.dag())

# Single Excitation Basis

I = tensor(qeye(2), qeye(2))
sz1 = tensor(sigmaz(), qeye(2))
sz2 = tensor(qeye(2), sigmaz())

HS2 = 0.5 * (eps1 + eps2) * qeye(2) + .5 * (eps1 - eps2) * sigmaz() + J12 * sigmax()

I = qeye(2)
a1 = tensor(a, I)
a2 = tensor(I, a)

g1 = sqrt(JJ(wa))
g2 = sqrt(JJ(wb))
#g1 = 0.001
#g2 = 0.002

X = g1 * ( a1 + a1.dag() ) + g2 * ( a2 + a2.dag() )


HI2 = X

HB2 =   2 * (wa * a1.dag() * a1 + wb * a2.dag() * a2)
                  
H2 = tensor(HS2, tensor(qeye(2), qeye(2))) + tensor(qeye(2),HI2) + tensor(qeye(2),HB2)

# I get excitons coefficients

exc1 = HS2.eigenstates()[1][0]
exc2 = HS2.eigenstates()[1][1]

U = numpy.matrix([[exc1[0,0], exc2[0,0]],[exc1[1,0], exc2[1,0]]])

###################################################################################

# time evolution

psi0 = 1/sqrt(2) * (zero + one)
psi0 = zero
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)
rho0.ptrace(0)

# sigmaz

times = linspace(0.0, 4.0e-1, 600.0)
result = mesolve(H2, rho0, times, [], [tensor(sigmaz(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig('sigmaz.png')
show()

# coherence in rdm

result = mesolve(H2, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho00_rdm.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho01_rdm.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_10 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho10_rdm.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm.png')
show()   
  
    
# whole system density matrix

times = linspace(0.0, 4.0e-1, 600.0) 
result = mesolve(H2, rho0, times, [], [])

x = []
for i in range(l):
    s = result.states[i][0,1]
    x.append(s)      

# rho 04

times = linspace(0.0, 40.0e-1, 600.0) 
result = mesolve(H2, rho0, times, [], [])

x1 = []
for i in range(l):
    s = result.states[i][0,4]
    x1.append(s)   
    
plt.plot(times, x1)
plt.ylabel('Rho_04')
plt.xlabel('Time')
savefig('rho04_systemplusbath.png')
show()  

times = linspace(0.0, 5.0e-1, 600.0) 
result = mesolve(H2, rho0, times, [], [])

x1 = []
for i in range(l):
    s = result.states[i][0,4]
    x1.append(s)   

plt.plot(times, x1)
plt.ylabel('Rho_04')
plt.xlabel('Time')
savefig('rho04_systemplusbath_detail.png')
show()      

# rho 23

times = linspace(0.0, 40.0e-1, 600.0) 
result = mesolve(H2, rho0, times, [], [])

x2 = []
for i in range(l):
    s = result.states[i][2,3]
    x2.append(s)       

plt.plot(times, x2)
plt.ylabel('Rho_23')
plt.xlabel('Time')
savefig('rho23_systemplusbath.png')
show()

times = linspace(0.0, 5.0e-1, 600.0) 
result = mesolve(H2, rho0, times, [], [])

x1 = []
for i in range(l):
    s = result.states[i][2,3]
    x1.append(s)   

plt.plot(times, x1)
plt.ylabel('Rho_23')
plt.xlabel('Time')
savefig('rho23_systemplusbath_detail.png')
show()      

# rho 11

times = linspace(0.0, 130.0e-1, 600.0) 
result = mesolve(H2, rho0, times, [], [])

x2 = []
for i in range(l):
    s = result.states[i][1,1]
    x2.append(s)       

plt.plot(times, x2)
plt.ylabel('Rho_11')
plt.xlabel('Time')
savefig('rho11_systemplusbath.png')
show()

times = linspace(0.0, 5.0e-1, 600.0) 
result = mesolve(H2, rho0, times, [], [])

x1 = []
for i in range(l):
    s = result.states[i][1,1]
    x1.append(s)   

plt.plot(times, x1)
plt.ylabel('Rho_11')
plt.xlabel('Time')
savefig('rho11_systemplusbath_detail.png')
show()   

