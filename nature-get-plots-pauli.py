#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  8 13:39:41 2017

@author: fcm
"""
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 17:31:10 2017

Nature Dimer Coupled to Bath - Pauli Matrices

@author: fcm
"""

from qutip import *
from pylab import *
from scipy import *
from math import factorial
import sympy
import os

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)


# working directory

os.chdir('/Users/fcm/qutip')


# state space

# |0> and |1> states 
zero = basis(2,0)
one = basis (2,1)

e1 = tensor(one, zero)
e2 = tensor(zero, one)

e = [e1,e2]


# parameters

eps = [10, 140]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])


# spectral densities

ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22
S1 = 5
S2 = 10

# I express quantities in SI units
h = 6.62606957e-34
hbar = 1.054571628e-34
c = 300000000
wa = 200 * pi * c * 0.57
wb = 200 * pi * c * 1.9
eps1 = 100 * h * c * 10
eps2 = 100 * h * c * 140
eps = [eps1,eps2]


def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    # JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    # quito background spectrum
    JJ = S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ



# mode displacement operator

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )


# HAMILTONIAN

zero = basis(2,0)
one = basis(2,1)

eps1 = eps[0]
eps2 = eps[1]
J12 = J[0,1]

# Single Excitation Basis


I = tensor(qeye(2), qeye(2))
sz1 = tensor(sigmaz(), qeye(2))
sz2 = tensor(qeye(2), sigmaz())


HS2 = 0.5 * (eps1 + eps2) * qeye(2) + .5 * (eps1 - eps2) * sigmaz() + J12 * sigmax()

X = g1 * ( a1 + a1.dag() ) + g2 * ( a2 + a2.dag() )

HI2 = X

HB2 =   2 * (wa * a1.dag() * a1 + wb * a2.dag() * a2)
                 
H2 = tensor(HS2, tensor(qeye(2), qeye(2))) + tensor(qeye(2),HI2) + tensor(qeye(2),HB2)


###################################################################################

# TIME EVOLUTION

# name + simulation parameters 

sub = 'equal-energy-hiper-long-'
times = linspace(0.0, 1e-7, 2000.0)

# time evolution starting in one localized excitation

psi0 = zero
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
# rhob = ket2dm(tensor(one, one))
rhob = tensor(thermal_dm(2,2),thermal_dm(2,2))
rho0 = tensor(rhos, rhob)
rho0.ptrace(0)

# sigmaz

result = mesolve(H2, rho0, times, [], [tensor(sigmaz(),qeye(2),qeye(2))])

plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig(sub + 'sigmaz_localized.png')
savefig('sigmaz_localized.png')
show()


# coherence in rdm

result = mesolve(H2, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho00_rdm_localized.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho01_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_10 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho10_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho11_rdm_localized.png')
show()   
  


# time evolution starting in triplet state

psi0 = 1/sqrt(2) * (zero + one)
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = tensor(thermal_dm(2,2),thermal_dm(2,2))
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)
rho0.ptrace(0)

# sigmaz

result = mesolve(H2, rho0, times, [], [tensor(sigmaz(),qeye(2),qeye(2))])

plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig(sub + 'sigmaz_triplet.png')
show()

# coherence in rdm

result = mesolve(H2, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho00_rdm_triplet.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho01_rdm_triplet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_10 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho10_rdm_triplet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho11_rdm_triplet.png')
show()   
  
# time evolution starting in singlet state

psi0 = 1/sqrt(2) * (zero - one)
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)
rho0.ptrace(0)

# sigmaz

result = mesolve(H2, rho0, times, [], [tensor(sigmaz(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig(sub + 'sigmaz_singlet.png')
show()



# coherence in rdm

result = mesolve(H2, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho00_rdm_singlet.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho01_rdm_singlet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_10 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho10_rdm_singlet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,1]
    x.append(s.real)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho11_rdm_singlet.png')
show()   
  


