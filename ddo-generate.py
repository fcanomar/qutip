#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 31 01:09:50 2018

Generate distribution of dipoles with a given degree of dipole orientation

@author: fcm
"""

# Generate random molecules

N = 50
l = 2

rm_v = []

for i in range(0,N):
    x = np.random.rand(3)*l
    rm_v.append(x.tolist())
    
# Generate random vectors with average dipole orientation
    
rp_v = []
p = 1

for i in range(0,N):
    x = [np.random.normal(),np.random.normal(),np.random.normal(1)]
    x = x/np.linalg.norm(x)
    rp_v.append(x.tolist())

soa = []

for i in range(0,N):
    x = list(rm_v[i])
    x.extend(rp_v[i])
    soa.append(x)
    x = []
    
# Plot     
    
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')  

X, Y, Z, U, V, W = zip(*soa)

ax.quiver(X, Y, Z, U, V, W)
ax.set_xlim([-2, 2])
ax.set_ylim([-2, 2])
ax.set_zlim([-2, 2])
plt.show()

# Degree of dipole orientation

p_x = 0
p_y = 0
p_z = 0

for i in range(0,N):
    p_x += rp_v[i][0]
    p_y += rp_v[i][1]
    p_z += rp_v[i][2]

p_x = p_x/N
p_y = p_y/N
p_z = p_z/N  