#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri May 26 18:54:40 2017

@author: fcm
"""

import matplotlib.pyplot as plt


delta = 0.2 * 2*np.pi; eps0 = 1.0 * 2*np.pi; gamma1 = 0.5

H = - delta/2.0 * sigmax() - eps0/2.0 * sigmaz()

def ohmic_spectrum(w):
    if w == 0.0: # dephasing inducing noise
        return gamma1
    else: # relaxation inducing noise
        return gamma1 / 2 * (w / (2 * np.pi)) * (w > 0.0)
    
R, ekets = bloch_redfield_tensor(H, [sigmax()], [ohmic_spectrum])

tlist = np.linspace(0, 15.0e-12, 1000)

psi0 = rand_ket(2)

e_ops = [sigmax(), sigmay(), sigmaz()]

expt_list = bloch_redfield_solve(R, ekets, psi0, tlist, e_ops)

expt_list1 = bloch_redfield_solve(R, ekets, psi0, tlist, [sigmax()])

plt.plot(tlist, expt_list1[0])


# Two Oscillators


sx1 = tensor(sigmax(), I)
sx2 = tensor(I, sigmax())
sz1 = tensor(sigmaz(), I)
sz2 = tensor(I, sigmaz())

H2 =  - delta/2.0 * sx1 - eps0/2.0 * sz1 - delta/2.0 * sx2 - eps0/2.0 * sz2

R, ekets = bloch_redfield_tensor(H2, [sx1 + sx2], [ohmic_spectrum])

tlist = np.linspace(0, 15.0, 1000)

psi0 = tensor(rand_ket(2), rand_ket(2))

e_ops = [sx1, sz2]

expt_list2 = bloch_redfield_solve(R, ekets, psi0, tlist, e_ops)

plt.plot(tlist, expt_list2[1])