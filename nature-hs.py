#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 24 18:04:54 2017

@author: fcm
"""

zero = basis(2,0)
one = basis (2,1)
vac = Qobj([[0],[0]])

e1 = tensor(zero, one)
e2 = tensor(one, zero)

e = [e1,e2]

# parameters

eps = [10, 140]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])


# Hamiltonian

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()
