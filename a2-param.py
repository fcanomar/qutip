#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 14:10:03 2020

DFT and SAE comparison

@author: fcm
"""

# creates: 2sigma.png, co_wavefunctions.png
import numpy as np
import matplotlib.pyplot as plt
from ase import Atoms
from ase.units import Bohr
from ase.io.cube import read_cube_data

from gpaw import GPAW
from gpaw.spherical_harmonics import Y

a = 8.0
L = 2 * a # O1 as zero

# s22 dimer positions
co = Atoms('OHHOHH', 
           positions=[(4., 3. , 3.),
           (3.70889836, 3.50935136, 2.241439  ),
           (3.70889836, 3.50935136, 3.758561  ),
           (6.91041909, 3.        , 3.        ),
           (5.94990784, 2.9191058 , 3.        ),
           (7.22441467, 2.09586604, 3.        )])

co.set_cell((2*a,a,a))
co.center()
co.calc = GPAW(mode='lcao',
               txt='CO.txt')
e = co.get_potential_energy()
L = 2 * co.get_positions()[0][0]
print(co.positions[:, 0] - L/2)

# DFT energy
# equilibrium position 8.48325132 -- 1.94990 with origin in O1
incr = np.linspace(-1.0,.5,20)
energy = []

for el in incr:
    co.positions[4][0] = 8.483 + el
    en = co.get_potential_energy()
    energy.append(en)
    
x = 8.483 + incr - L/2
plt.plot(x,energy)
plt.savefig('DFT-energy.png')

# Fit with Harmonic (second order) polynomial

plt.plot(x[8:14],energy[8:14])
np.polyfit(x[8:14],energy[8:14],2)

import numpy.polynomial.polynomial as poly

x_new = np.linspace(x[8],x[14],100)

coefs = poly.polyfit(x[8:14],energy[8:14],2)
ffit = poly.polyval(x_new, coefs)
plt.plot(x_new, ffit)
    
