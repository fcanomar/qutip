#!/usr/bin/env python

import sys

sys.path.insert(0, '~/qutip/DVR-Fourier-1D/')

import numpy as np
import matplotlib.pyplot as plt

import logging

import numpy as np
import scipy.linalg

from scipy.constants import hbar

class Domain_Fourier_DVR_1D(object):
    """
    Solves the Schroedinger equation on a finite one-dimensional interval using
    the discrete variable representation method with the Fourier sine basis:

    phi_k = sqrt(2 / (b-a)) * sin(k * pi * (x-a) / (b-a)), k=1..n_DVR

    For details, see for example Appendix 2 of:
    J. Chem. Phys. 104, 8433 (1996)
    http://dx.doi.org/10.1063/1.471593
    """

    def __init__(self, a, b, n_DVR):
        """Constructs the domain with given end points and basis size.

        :param a: lower bound of domain
        :param b: upper bound of domain
        :param n_DVR: number of basis functions

        """

        # store domain parameters
        self._a = a
        self._b = b
        self._n_DVR = n_DVR

        # no previous calculation was performed
        self._m = None
        self._V = None
        self._X = None

        # update spectral decomposition of the position operator
        self._update_X_in_Fourier_basis()

    def _update_X_in_Fourier_basis(self):
        """Decompose the position operator in the Fourier sine basis.

        Eigenvalues and eigenvectors of the position operator
        in the Fourier sine basis are stored in `self`.

        """

        logging.debug('X decomposition | start')

        a = self._a
        b = self._b
        n_DVR = self._n_DVR

        # construct position operator in Fourier sine basis
        i = np.arange(1, n_DVR+1, dtype=float)[:, np.newaxis].repeat(n_DVR, axis=1)
        j = i.transpose()
        ipj = i + j
        ipjmod1 = (ipj) % 2
        div = np.where(ipjmod1,  ((i-j) * ipj)**2, 1)
        fact = -8.0 * (b-a) / np.pi**2
        X_four = fact * np.where(ipjmod1, (i*j) / div, 0.0)

        x, phi_four = scipy.linalg.eigh(X_four)
        x = x + (a+b) / 2.0
        
        self._X = X_four # I also store position operator matrix
        self._x = x
        self._phi_four = phi_four

        logging.debug('X decomposition | done')

    def _update_T_four(self, m):
        """Build the kinetic energy operator and store it in `self`.

        :param m: mass

        """

        if self._m is None or (m != self._m):

            self._m = m

            l = self._b - self._a
            t = (0.5 / m) * (np.pi / l)**2 * np.arange(1, self._n_DVR + 1)**2
            self._T_four = np.diagflat(t)

            logging.debug('build T | done')

    def _update_V_four(self, V):
        """Build the potential energy operator and store it in `self`.

        :param V: potential energy - real-space vectorized function

        """

        if self._V is None or (V != self._V):

            self._V = V

            phi_four = self._phi_four
            V_x = np.diagflat(V(self._x))
            self._V_four = np.dot(np.dot(phi_four, V_x), phi_four.transpose())

            logging.debug('build V | done')

    def solve(self, m, V, n_states=None, calc_eigenstates=True):
        """Solve the Schroedinger equation on this domain.

        :param m: mass
        :param V: potential energy - real-space vectorized function
        :param n_states: number of states to calculate
        "param calc_eigenstates: whether to return eigenstates as well

        Returns eigenenergies and (optionally) eigenstates of the Hamiltonian
        sorted by eigenenergy magnitude.

        """

        logging.debug('solve | start')

        if n_states is None:
            eigvals = None
        else:
            eigvals = (0, n_states)

        # update kinetic energy and potential operators, if needed
        self._update_T_four(m)
        self._update_V_four(V)

        # construct the Hamiltonian
        H_four = self._T_four + self._V_four
        logging.debug('solve | Hamiltonian built')

        # solve
        eigvals_only = not calc_eigenstates
        result = scipy.linalg.eigh(H_four,
                                   eigvals=eigvals,
                                   eigvals_only=eigvals_only,
                                   overwrite_a=True)
        logging.debug('solve | done')

        return result


    def grid(self, x, psi_four):
        """Evaluate states on a real-space grid.

        :param x: real-space grid - 1D array
        :param psi_four: states in Fourier basis

        Returns states on grid x.

        """

        logging.debug('grid | start')

        n_DVR, n_out = psi_four.shape

        if n_DVR != self._n_DVR:
            data = (self._n_DVR, n_DVR)
            err = 'Wrong dimension of states. Expected %d, got %d.' % data
            raise ValueError(err)

        # convenience
        a = self._a
        b = self._b

        # flag points outside the [a, b] interval
        outside = np.logical_and((x > a), (x < b))

        # construct Fourier sine basis functions on real-space grid
        norm = np.sqrt(2 / (b-a))
        four_1_grid = np.exp(1.0j * np.pi * (x-a) / (b-a))
        four_1_grid *= outside
        k = np.arange(1, n_DVR + 1, dtype=complex)
        four_grid = norm * np.imag(four_1_grid[:, np.newaxis]**k)

        # project states to real-space grid
        psi_grid = np.dot(four_grid, psi_four).transpose()

        logging.debug('grid | done')

        return psi_grid



#===========================================================================================================================================================================================================================================
# Example: Different Potentials V(x)
#===========================================================================================================================================================================================================================================
        
# settings
m = 1.0
x_min = -5.0
x_max = 5.0
n_DVR = 10
n_g = 50
D0 = 43.0
a = 1.5


# Ven 

Z = 6
r0 = 1.0
alpha = 1.0

def Ven(r):
    return -(.5 + (Z -1/2))*np.exp(-r/r0)/np.sqrt(r**2 + alpha**2)


# Vnn
   
De = 1.0
A = 1.0
R = 1.0
R0 = 1.0

def Vnn(R):
    return De*(1-np.exp(A*(R-R0)))**2
    

# V

E0 = 0.0
R = 1.0

def V(x):
    # return (- Ven(np.abs(x-R/2))) 
    return (E0 - Ven(np.abs(x-R/2)) - Ven(np.abs(x+R/2)) + Vnn(R))

# V = lambda x: 10 * np.exp(-x ** 2) + x ** 2

# V = lambda x: 0.5 * k * x * x

# Hydrogen Atom 1D

#def V(x):
#    if x > 0:
#        V = - 1 / x
#    else:
#        V = 1e10
#    return V

# V = lambda x: 1 / x

n_plot = 10
scale = 4.0

# solve
domain = Domain_Fourier_DVR_1D(x_min, x_max, n_DVR)
E, E_four = domain.solve(m, V)

# evaluate eigenstates on grid
x = np.linspace(x_min, x_max, n_g)
psi_x = domain.grid(x, E_four[:,:n_plot])

# print energies
for i, e in enumerate(E[:n_plot]):
    print '%3i %12.6f' % (i, e)

# plot eigenstates
plt.figure()
plt.subplots_adjust(left=0.05, right=0.95,
                    bottom=0.05, top=0.95)
plt.plot(x, V(x), 'k-', lw=2)
for i in range(n_plot):
    plt.plot([x[0], x[-1]], [E[i], E[i]], '--', color='gray')
for i in range(n_plot):
    plt.plot(x, scale * psi_x[i] + E[i])
plt.xlim(x_min, x_max)
#plt.ylim(-E[0], E[n_plot])
plt.show()


#===========================================================================================================================================================================================================================================
# Obtain B.O.A. Potential Curves
#===========================================================================================================================================================================================================================================

# V

E0 = 0.0
R = 1.0

R_v = np.linspace(0.3, 1.5, 10)
E0_v = []
E1_v = []

for R in R_v:
    
    # a lo bestia para no cambiar Domain_Fourier...
    def V(x):
        # return (- Ven(np.abs(x-R/2))) 
        return (E0 - Ven(np.abs(x-R/2)) - Ven(np.abs(x+R/2)) + Vnn(R))

    domain = Domain_Fourier_DVR_1D(x_min, x_max, n_DVR)
    E, E_four = domain.solve(m, V)
    E0_v.append(E[0])
    E1_v.append(E[1])
    
plt.figure()  
plt.plot(R_v, E0_v)
plt.plot(R_v, E1_v)
plt.show()

#===========================================================================================================================================================================================================================================
# Include Photon Interaction I: Obtain Hamiltonian Matrix
#===========================================================================================================================================================================================================================================

# ME Solver for Hm (preliminary test: formats)

from qutip import *

H_four = np.asarray(domain._T_four + domain._V_four)
H = Qobj(H_four)

psi0 = basis(10,0)

times = np.linspace(0.0, 50.0, 300.0)

result = mesolve(H, psi0, times, [], [])

# Here I want to obtain V(R) including the photon interaction

H.eigenenergies()

# V

R = 1.0

R_v = np.linspace(0.3, 1.5, 10)
E0_v = []
E1_v = []

a = destroy(2)
wc = 100.0
g = 100.0

for R in R_v:
    
    # a lo bestia para no cambiar Domain_Fourier...
    def V(x):
        # return (- Ven(np.abs(x-R/2))) 
        return (Ven(np.abs(x-R/2)) - Ven(np.abs(x+R/2)) + Vnn(R))

    domain = Domain_Fourier_DVR_1D(x_min, x_max, n_DVR)
    E, E_four = domain.solve(m, V)
    
    # Include photon interaction
    
    H_four = np.asarray(domain._T_four + domain._V_four)
    Hm = Qobj(H_four)
    
    H_four = np.asarray(domain._X)
    X = Qobj(H_four)
    
    H = tensor(Hm,qeye(2)) + tensor(qeye(n_DVR), wc * a.dag() * a) + g * tensor(X, a.dag() + a)


    E0_v.append(H.eigenenergies()[0])
    E1_v.append(H.eigenenergies()[1])


plt.figure()  
plt.plot(R_v, E0_v)
plt.plot(R_v, E1_v)
plt.show()

