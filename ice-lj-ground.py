#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 15 10:51:06 2019

Ice Leonard Jones Ground State Calculation and Effects of Confinement on Electron Density

@author: fcm
"""

from ase.units import Bohr
from gpaw import GPAW, PW, restart
from gpaw.external import ExternalPotential
from ase import Atoms, Atom
import numpy as np
from ase.calculators.tip4p import TIP4P, rOH, angleHOH
from math import pi, cos, sin
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.constraints import FixBondLengths, FixAtoms
from ase.md import Langevin

from ase.visualize import view

import plot_electron_density as pled

#-------------------------------------------------------------------------------------------------------------------
# I set the ice sheet and the internal water chain
#-------------------------------------------------------------------------------------------------------------------

# I add a central water molecule

x = angleHOH * np.pi / 180 / 2

pos = [[0, 0, 0],
       [rOH, 0, 0],
       [rOH * np.cos(x), 0 , rOH * np.sin(x)]]
       

# atoms = Atoms('OH2', positions=pos)

atoms  = Atoms()

# I add the rest of the molecules forming an octogon

d = 2.8
n = 6
theta = 2 * pi / n
dist = d/2/sin(theta/2)
#n = 1
for i in range(1, n+1):
        atoms_aux = Atoms()
        x0 = dist * cos(-i*theta)
        y0 = dist * sin(-i*theta)
        pos = [[x0, y0, 0],
               [x0, y0, rOH],
               [x0 + rOH * np.sin(x), y0 , rOH * np.cos(x)]]
        atoms_aux.append(Atom('O', pos[0]))
        atoms_aux.append(Atom('H', pos[1]))
        atoms_aux.append(Atom('H', pos[2]))
        #atoms_aux.rotate('z',30 + 60 * i,(x0, y0, 0))
        #atoms_aux.rotate('z',30 * i + 150,(x0, y0, 0))
        atoms_aux.rotate(a=-(120 + 60 * i),v='z',center=(x0, y0, 0))
        [atoms.append(atoms_aux[i]) for i in range(3)]

# I set the cell for the unit
       
a = 3

atoms.set_cell((5*a+0.001, 5*a, 2.8)) 
atoms.center()

# I repeat along the z axis N times - separation 2.8 angstroms

N = 3

#-> undo!! #atoms = atoms.repeat((1, 1, N))

# Set cell for the whole

atoms.set_cell((5*a+0.001 , 5*a, 2.8*N))

atoms.center()

# Set boundary conditions

atoms.set_pbc((False, False, True))


#-------------------------------------------------------------------------------------------------------------------
# I constrain the Oxygen atoms and use Langevin dynamics to induce disorder in the protons
#-------------------------------------------------------------------------------------------------------------------

# Fixed Oxygens
    
indices = [atom.index for atom in atoms if atom.symbol == 'O']

l = FixAtoms(indices)

c = []
c.append(l)

# Rattle-type bond lenghts constraints on O-H1 O-H2 and H1-H2

Nt = 9 * N
c2 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(Nt)
                                   for j in [0, 1, 2]]) 
c.append(c2)

# I add both type of constraints

atoms.set_constraint(c)

# I set the calculator

atoms.calc = TIP4P(rc=3.5)

# I use Langevin dynamics to add disorder to the H positions

tag='ice-lj-ground'

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
              friction=0.01, logfile=tag + '-langevin.log') # no log file, add logfile=tag + '-langevin.log' to export logfile

traj = Trajectory(tag + '-langevin.traj', 'w', atoms)

md.attach(traj.write, interval=1)

md.run(50)

#-------------------------------------------------------------------------------------------------------------------
# I define a Leonard Jones potential to model the interaction with the Nanotube surface
#-------------------------------------------------------------------------------------------------------------------     

eps = 0.005 # depth of the well
sigma = 3.8 # distance for V=0
D = 14 # nanotube diameter
class LJ_Potential(ExternalPotential):
    def calculate_potential(self, gd):
        r_vg = gd.get_grid_point_coordinates()
        r = ((r_vg[1] - a / Bohr / 2)**2 + (r_vg[2] - a / Bohr / 2)**2)**.5
        self.vext_g = 4 * eps * ((sigma/(D-r))^12-(sigma/(D-r))^6)  

    def todict(self):
        return {}

#-------------------------------------------------------------------------------------------------------------------
# I do a ground state calculation
#-------------------------------------------------------------------------------------------------------------------

atoms.set_constraint([])

calc = GPAW(external=LJ_Potential(), mode=PW(100))
# calc = GPAW()
atoms.calc = calc


# calc.attach(calc.write, 1, tag + '.gpw', mode='all')

# ---uncomment to run calculation
atoms.get_potential_energy() 

#-------------------------------------------------------------------------------------------------------------------
# I obtain the electron density and represent
#-------------------------------------------------------------------------------------------------------------------

n = calc.get_all_electron_density()
# atoms, calc = restart(tag + '.gpw')

pled.plot_electron_density_xy(atoms)

# write cube file
# atoms.write(tag + '.cube', format='cube')

#-------------------------------------------------------------------------------------------------------------------
# I plot the Grid
#-------------------------------------------------------------------------------------------------------------------

# x = numpy.arange(0, 1, 0.05)
# y = numpy.power(x, 2)

fig = plt.figure(figsize=(20,20))
ax = fig.gca()
ax.set_xticks(np.linspace(0, atoms.cell[0,0],n.shape[0]))
ax.set_xticklabels(np.round(np.linspace(0, atoms.cell[0,0],n.shape[0]),2), rotation='vertical')
ax.set_yticks(np.linspace(0, atoms.cell[1,1],n.shape[1]))
# plt.scatter(x, y)
plt.grid()
plt.show()

#-------------------------------------------------------------------------------------------------------------------
# I write in a Cube file
#-------------------------------------------------------------------------------------------------------------------

# write cube file
#atoms.write(tag + '.cube', format='cube')