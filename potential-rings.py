#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 02:40:53 2018

Potential created in the water rings

@author: fcm
"""

import sympy as sp
import numpy as np
import matplotlib.pyplot as plt

z = sp.Symbol('z')
a = sp.Symbol('a')

sp.integrate(.5*(z**2 + a**2)**-.5, z)

z_vect = np.linspace(-3,3,30)

def v(z,a):
    v = 0.282094791773878*np.sqrt(np.pi)*sp.asinh(z/a)
    return v

a = 3

v_ring = []
v_ring_inv = []
v_dip = []
v_tot = []

for i in range(0,len(z_vect)): 
    v_ring.append(v(z_vect[i],a))
    v_ring_inv.append(v(-z_vect[i],a))
    v_dip.append(1.0/3250.0/np.abs(z_vect[i])**3) # scaled down factor of 25, displaced 1.5
    v_tot.append(v_ring[i] + v_dip[i])

plt.plot(z_vect,v_ring)
plt.plot(z_vect,v_dip)
plt.plot(z_vect,v_tot)

plt.plot(v_ring + v_ring_inv + v_ring + v_ring_inv)
plt.plot(v_dip + v_dip + v_dip + v_dip)