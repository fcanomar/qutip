#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 10 12:22:29 2017

Based on Olaya-Castro, 2008

@author: fcm
"""

from qutip import *
from pylab import *
# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)

M=3
N=1
epsa = 1
gamm = 1
J = 1
g = 1

# M donors, N acceptors

si = qeye(2); sp = sigmap(); sm = sigmam()

sp_list = []; sm_list = [];
for n in range(M+N):
    op_list = [si for m in range(M+N)]
    op_list[n] = sp
    sp_list.append(tensor(op_list))
    op_list[n] = sm
    sm_list.append(tensor(op_list))


#HAMILTONIAN

#single particle
Hs = 0
for n in range(M+N):
    Hs += epsa * sp_list[n] * sm_list[n]
    
#donor-acceptor
Hda= 0
for j in range(M):
    for c in range(M+1,M+N) :
        Hda =+ gamm * (sp_list[j] * sm_list[c] + sp_list[c] * sm_list[j])
        
#donor-donor
Hdd= 0
for j in range(M):
    for k in range(M):
        if k>j :     
            Hdd =+ J * (sp_list[j] * sm_list[k] + sp_list[k] * sm_list[j])        
          
#acceptor-acceptor
Haa= 0
for c in range(M+1, M+N):
    for r in range(M+1,M+N):
        if r > c :
            Haa =+ gamm * (sp_list[j] * sm_list[c] + sp_list[c] * sm_list[j])
            

H = Hs + Hda + Hdd + Haa


# BASIS VECTORS N=1 M=3

zero = basis(2,0)
one = basis (2,1)

e1 = tensor(zero, zero, zero, zero)
e2 = tensor(zero, zero, zero, one)
e3 = tensor(zero, zero, one, zero)
e4 = tensor(zero, zero, one, one)
e5 = tensor(zero, one, zero, zero)
e6 = tensor(zero, one, zero, one)
e7 = tensor(zero, one, one, zero)
e8 = tensor(zero, one, one, one)
e9 = tensor(one, zero, zero, zero)
e10 = tensor(one, zero, zero, one)
e11 = tensor(one, zero, one, zero)
e12 = tensor(one, zero, one, one)
e13 = tensor(one, one, zero, zero)
e14 = tensor(one, one, zero, one)
e15 = tensor(one, one, one, zero)
e16 = tensor(one, one, one, one)



