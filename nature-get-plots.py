#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 17:31:10 2017

Nature Dimer Coupled to Bath - Pauli Matrices

@author: fcm
"""

from qutip import *
from pylab import *
from scipy import *
from math import factorial
import sympy
import os

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)


# working directory

os.chdir('/Users/fcm/qutip')


# state space

# |0> and |1> states 
zero = basis(2,0)
one = basis (2,1)

e1 = tensor(one, zero)
e2 = tensor(zero, one)

e = [e1,e2]


# parameters

eps = [140, 140]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])


# spectral densities

ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22

def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    #JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    # quito background spectrum
    JJ = S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ



# mode displacement operator

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )


# HAMILTONIAN

zero = basis(2,0)
one = basis(2,1)

eps1 = eps[0]
eps2 = eps[1]
J12 = J[0,1]

# Single Excitation Basis


I = tensor(qeye(2), qeye(2))
sz1 = tensor(sigmaz(), qeye(2))
sz2 = tensor(qeye(2), sigmaz())


HS2 = 0.5 * (eps1 + eps2) * qeye(2) + .5 * (eps1 - eps2) * sigmaz() + J12 * sigmax()

# we can simplify

# eps1 = 10
# eps2= 140

# HS2 = - (eps1 - eps2) * sigmax() + J12 * sigmaz()

# TEMPORAL EVOLUTION OF THE ISOLATED SYSTEM

psi0 = zero
rho0 = ket2dm(psi0)

# sigmaz

times = linspace(0.0, 200.0, 200.0)
result = mesolve(HS2, rho0, times, [], [sigmaz()])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig('sigmaz_ISOLATED_localized.png')
show()



I = qeye(2)
a1 = tensor(a, I)
a2 = tensor(I, a)


g1 = sqrt(JJ(wa))
g2 = sqrt(JJ(wb))
#g1 = 0.001
#g2 = 0.002


X = g1 * ( a1 + a1.dag() ) + g2 * ( a2 + a2.dag() )


HI2 = X


HB2 =   2 * (wa * a1.dag() * a1 + wb * a2.dag() * a2)
    
              
H2 = tensor(HS2, tensor(qeye(2), qeye(2))) + tensor(qeye(2),HI2) + tensor(qeye(2),HB2)


# System's hamiltonian in |00> |01> ... basis

e1 = tensor(zero, one)
e2 = tensor(one, zero)


HS = eps1 * e1 * e1.dag() + eps2 * e2 * e2.dag() + J12 * (e1 * e2.dag() + e2 * e1.dag())

HI = X * (e1 * e1.dag() + e2 * e2.dag())

HB =   2 * (wa * a1.dag() * a1 + wb * a2.dag() * a2)

H = HS + HI + HB

###################################################################################

# TIME EVOLUTION

# time evolution starting in one localized excitation

sub = 'EQUAL-'

psi0 = zero
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
# rhob = ket2dm(tensor(one, one))
rhob = tensor(thermal_dm(2,2),thermal_dm(2,2))
rho0 = tensor(rhos, rhob)
rho0.ptrace(0)

# sigmaz

times = linspace(0.0, 1.0, 100.0)
result = mesolve(H2, rho0, times, [], [tensor(sigmaz(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig(sub + 'sigmaz_localized.png')
savefig('sigmaz_localized.png')
show()



# coherence in rdm

result = mesolve(H2, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
# plt.ylabel('rho_00 (reduced density matrix)')
plt.suptitle('rho11')
plt.xlabel('Time')
savefig(sub + 'rho00_rdm_localized.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.suptitle('rho12')
# plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho01_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,0]
    x.append(s)

plt.plot(times, x)
plt.suptitle('rho21')
# plt.ylabel('rho_10 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho10_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.suptitle('rho22')
#plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho11_rdm_localized.png')
show()   
  



# time evolution starting in triplet state

psi0 = 1/sqrt(2) * (zero + one)
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = tensor(thermal_dm(2,2),thermal_dm(2,2))
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)
rho0.ptrace(0)

# sigmaz

times = linspace(0.0, 1.0, 1000.0)
result = mesolve(H2, rho0, times, [], [tensor(sigmaz(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig(sub + 'sigmaz_triplet.png')
show()



# coherence in rdm

result = mesolve(H2, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.suptitle('rho22')
plt.xlabel('Time')
savefig('detail-rho00_rdm_triplet.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho01_rdm_triplet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_10 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho10_rdm_triplet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho11_rdm_triplet.png')
show()   
  
# time evolution starting in singlet state

psi0 = 1/sqrt(2) * (zero - one)
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)
rhob = ket2dm(tensor(one, one))
rho0 = tensor(rhos, rhob)
rho0.ptrace(0)

# sigmaz

times = linspace(0.0, 10.0e-1, 600.0)
result = mesolve(H2, rho0, times, [], [tensor(sigmaz(),qeye(2),qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig(sub + 'sigmaz_singlet.png')
show()



# coherence in rdm

result = mesolve(H2, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho00_rdm_singlet.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[0,1]
    x.append(s)

plt.plot(times, x)
plt.plot(times, 5*exp(-times/150))
plt.ylabel('rho_01 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho01_rdm_singlet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_10 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho10_rdm_singlet.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace(0)
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig(sub + 'rho11_rdm_singlet.png')
show()   
  


