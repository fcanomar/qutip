#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  7 10:57:03 2018

@author: fcm
"""
import sys
sys.path.insert(0, '/Users/fcm/qutip/FSSH')

import fssh
import tullymodels as models

simple_model = models.TullySimpleAvoidedCrossing()

fssh.TrajectorySH.box_bounds = 4 # quit the simulation once the particle leaves the box [-4,4]
fssh.TrajectorySH.nsteps = 1000 # quit the simulation after 10000 time steps

# Generates trajectories always with starting position -5, starting momentum 2, on ground state
traj_gen = fssh.TrajGenConst(-5.0, 9.0, "ground")

simulator = fssh.BatchedTraj(simple_model, traj_gen, samples = 20)
results = simulator.compute()
outcomes = results.outcomes

print("Probability of reflection on the ground state:    %12.4f" % outcomes[0,0])
print("Probability of transmission on the ground state:  %12.4f" % outcomes[0,1])
print("Probability of reflection on the excited state:   %12.4f" % outcomes[1,0])
print("Probability of transmission on the excited state: %12.4f" % outcomes[1,1])