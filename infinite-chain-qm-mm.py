#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  2 10:50:41 2018

QM/MM Simulation of a Water Chain in a Nanotube

@author: fcm
"""

# The following script will calculate the QM/MM single point energy 
# of the water dimer from the S22 database of weakly interacting dimers 
# and complexes, using LDA and TIP3P, for illustration purposes.


from __future__ import print_function
from ase.data import s22
from gpaw import GPAW
from ase.build import nanotube
from ase.calculators.qmmm import SimpleQMMM
from ase.md import Langevin
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.calculators.tip3p import TIP3P
from ase.md.verlet import VelocityVerlet

from ase.calculators.emt import EMT
# from asap3 import EMT

tag = 'water-dimer-tube'

# QM Part: Water Dimer

# Create system
atoms = s22.create_s22_system('Water_dimer')
atoms.center(vacuum=8.0)
atoms.euler_rotate(90,90,0)
atoms.center()

# MM part: Nanotube

gpts = (40, 40, 88)

sys = nanotube(6, 0, length=4)
sys.set_cell((8, 8+0.001 , 20))
sys.center()
# sys.calc = GPAW(gpts=gpts)

sys.extend(atoms)

# Make QM atoms selection of the two water molecules
n = sys.get_number_of_atoms()
qm_idx = range(n - 6,n)


# Set up calculator
sys.calc = SimpleQMMM(qm_idx,
                    GPAW(txt=tag + '.out'),
                    EMT(),
                    EMT(),
                    vacuum=4,  # if None, QM cell = MM cell
                    )

print(sys.get_potential_energy())



# Molecular Dynamics

# Velocity Verlet

dyn = VelocityVerlet(sys, 5 * units.fs)
traj = Trajectory(tag + '.traj', 'w', atoms)
dyn.attach(traj.write, interval=1)
dyn.run(400)


# Langevin Dynamics

#md = Langevin(sys, 1 * units.fs, temperature=300 * units.kB,
#              friction=0.01, logfile=tag + '.log')
#
#traj = Trajectory(tag + '.traj', 'w', atoms)
#md.attach(traj.write, interval=1)
#md.run(400)

