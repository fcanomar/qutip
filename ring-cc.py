#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  9 18:28:57 2019

Water Ring HF, CC

@author: fcm
"""

from ase.units import Bohr
from gpaw import GPAW, PW
from gpaw.external import ExternalPotential
from ase import Atoms, Atom
import numpy as np
from ase.calculators.tip4p import TIP4P, rOH, angleHOH
from math import pi, cos, sin
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.constraints import FixBondLengths, FixAtoms
from ase.md import Langevin

from ase.visualize import view

tag='ring3'

#-------------------------------------------------------------------------------------------------------------------
# I set the ice sheet and the internal water chain
#-------------------------------------------------------------------------------------------------------------------

atoms = Atoms()

# I add the molecules forming a polygon of n sides

d = 2.8
n = 3 
theta = 2 * pi / n
dist = d/2/sin(theta/2)

for i in range(1, n+1):
        x0 = dist * cos(i*theta)
        y0 = dist * sin(i*theta)
        pos = [[x0, y0, 0],
               [x0, y0, rOH],
               [x0 + rOH * np.sin(x), y0 , rOH * np.cos(x)]]
        atoms.append(Atom('O', pos[0]))
        atoms.append(Atom('H', pos[1]))
        atoms.append(Atom('H', pos[2]))
        # atoms[:-3].rotate(theta,'z')

# I set the cell for the unit
       
a = 3

atoms.set_cell((5*a+0.001, 5*a, 5*a+0.0008)) 
atoms.center()


#-------------------------------------------------------------------------------------------------------------------
# I constrain the Oxygen atoms and use Langevin dynamics to induce disorder in the protons
#-------------------------------------------------------------------------------------------------------------------

# Fixed Oxygens
    
indices = [atom.index for atom in atoms if atom.symbol == 'O']

l = FixAtoms(indices)

c = []
c.append(l)

# Rattle-type bond lenghts constraints on O-H1 O-H2 and H1-H2

# Nt = 9 * N
c2 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(n)
                                   for j in [0, 1, 2]]) 
c.append(c2)

# I add both type of constraints

atoms.set_constraint(c)

# I set the calculator

atoms.calc = TIP4P(rc=3.5)

# I use Langevin dynamics to add disorder to the H positions

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
              friction=0.01, logfile=tag + '-langevin.log') # no log file, add logfile=tag + '-langevin.log' to export logfile

traj = Trajectory(tag + '-langevin.traj', 'w', atoms)

md.attach(traj.write, interval=1)

md.run(50)

#-------------------------------------------------------------------------------------------------------------------
# I do a Ground State calculation in the LCAO Basis
#-------------------------------------------------------------------------------------------------------------------

atoms.set_constraint([])

calc = GPAW(mode='lcao', basis='dzp')

atoms.set_calculator(calc)

atoms.get_potential_energy()
