#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 31 15:11:01 2018

Calculate State Functions with Water Aligned Dipoles

@author: fcm
"""
import math
from ase.visualize import view
from ase.constraints import FixBondLengths
from ase.io.trajectory import Trajectory, TrajectoryReader

# get final state from trajectory file
atoms = TrajectoryReader('tip4p_27mol_equil.traj')
atoms = atoms[-1]
N = len(atoms)/3


# RATTLE-type constraints on O-H1, O-H2, H1-H2.
atoms.constraints = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(3**3)
                                   for j in [0, 1, 2]])

# Get list of centers of mass of each water molecules 

com_v = []

for i in range(0,N):
    mol = atoms[0+3*i:3+3*i]
    com_v.append(mol.get_center_of_mass().tolist())
       
# Generate random vectors with average dipole orientation
    
rp_v = []
p = 1

for i in range(0,N):
    x = [np.random.normal(),np.random.normal(),np.random.normal(1)]
    x = x/np.linalg.norm(x)
    rp_v.append(x.tolist())   
    
# Rotate molecules according to a list of directions


# Rotate molecules to match given dipole vectors
v1 = [1,1,1]
v2 = [2,1,1]

axis = np.cross(v1,v2)
angle = np.arccos(np.dot(v1,v2)/np.linalg.norm(v1)/np.linalg.norm(v2))
    

    
for i in range(0,N):
    
    mol = atoms[0+3*i:3+3*i]
    
    # Get TIP4P dipole vector
    
    v1 = mol.get_positions()[1]-mol.get_center_of_mass()
    v2 = mol.get_positions()[2]-mol.get_center_of_mass()
    v = (v1 + v2)/np.linalg.norm(v1 + v2)    
    
    # Rotate to match given dipole vector
    rp = [0,0,1]   
    axis = np.cross(v,rp)
    angle = np.arccos(np.dot(rp,v)/np.linalg.norm(rp)/np.linalg.norm(v))
    
    mol.rotate(axis,math.degrees(angle),center='COM') 
    
    mol = atoms[0+3*i:3+3*i]
    



 