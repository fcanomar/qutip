#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Nov  9 12:49:11 2019

CSMA/CA throughput as a function of the total channel arrival rate for different values of the buffer capacity

@author: fcm
"""

from numpy import *
from random import randrange
import matplotlib.pyplot as plt

# PARAMETERS 

Tf = 1000 # total simulation time
T = 4 # packet size (number of time slots)
N = 100 # number of nodes
b = 40 # buffer size (packets)
G = 10.0 # total arrival rate (packets/slot)

# SIMULATION ELEMENTS

A = zeros(N,dtype='int')

# update nodes (one time slot)

def update_nodes(A, b, l):
    for i in range(N):
        p = poisson(l)
        while p > abs(A[i] - b): 
            p = p - 1
        A[i] = A[i] + p   
    return A

# select transmiter
    
def select_transmitter(A):
    A_loaded = where(A > 0)[0]
    if len(A_loaded)>0:
        i = A_loaded[randrange(len(A_loaded))]
    else:
        i = None
    return i
        

# RUN SIMULATION

def run_simulation(A=A, Tf=Tf, T=T, N=N, b=b, G=G):
    t = 0
    P = 0
    l = G/N # Poisson lambda per node
    A = zeros(N,dtype='int')
    while (t<Tf):
        i = select_transmitter(A)
        print('Node selected : ' + str(i))
        if i is not None:
            print('t = ' + str(t))
            P += 1
            A[i] = A[i] - 1
            for tm in range(T):
                print('Node ' + str(i) + ' is transmitting')
                A = update_nodes(A,b,l)
                print('Update nodes: New A is ' + str(A)) 
                t += 1
                print('t = ' + str(t))
            print('Packet transmitted: P = ' + str(P))    
        else:
            print('t = ' + str(t))
            print('No packets to transmit')
            A = update_nodes(A,b,l)
            print('Update nodes: New A is ' + str(A)) 
            t += 1
            print('t = ' + str(t))
            
    S = float(P*T)/float(Tf)   
    print('Throughput is ' + str(P) + ' packets of length ' + str(T) + '. Simulation time is ' + str(Tf) + '. Throughput is ' + str(S) + '.')
     
    return S

def run_simulation_quiet(A=A, Tf=Tf, T=T, N=N, b=b, G=G):
    t = 0
    P = 0
    l = G/N # Poisson lambda per node
    A = zeros(N,dtype='int')
    while (t<Tf):
        i = select_transmitter(A)
        if i is not None:
            P += 1
            A[i] = A[i] - 1
            for tm in range(T):
                A = update_nodes(A,b,l)
                t += 1
        else:
            A = update_nodes(A,b,l)
            t += 1
        
    S = float(P*T)/float(Tf)    
   
    return S
           

# PLOT RESULTS

G_vector = linspace(0,1,50) # total arrival rate (packets/slot)
l_vector = G_vector/N
B_vector = [1,10,20,40] # buffer sizes (packets)

S_out = []
SB_out = []

for m in range(len(B_vector)):
    for k in range(len(G_vector)):
        S_out.append(run_simulation_quiet(G=G_vector[k], b=B_vector[m]))
    SB_out.append(S_out)
    S_out = []

plt.plot(G_vector, S_out)

fig = plt.figure()
plt.xlabel("Total arrival rate (packets/slot)")
plt.ylabel("Throughput (packets * length / total time slots)")
plt.title("CSMA/CA throughput")
for i in range(len(SB_out)):
    #plt.plot(G_vector,[S_out[i] for pt in SB_out],label = 'buffer size (packets) %s'%B_vector[i])
    plt.plot(G_vector, SB_out[i], label = 'Buffer size (packets) =  %s'%B_vector[i])
plt.legend()
plt.show()
fig.savefig('throughput.png')