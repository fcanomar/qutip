#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 25 09:08:42 2017

Interaction Hamiltonian

@author: fcm
"""

# general case

g1 = 1
g2 = 5

a = destroy(2)
I = qeye(2)

a1 = tensor(I, I, a, I)
a2 = tensor(I, I, I, a)

e1 = tensor(zero, zero, zero, zero)
e2 = tensor(zero, zero, zero, one)
e3 = tensor(zero, zero, one, zero)
e4 = tensor(zero, zero, one, one)

X = g1 * ( a1 + a1.dag() ) + g2 * ( a2 + a2.dag() )

X * e1 == g1 * e3 + g2 * e2
X * e2 == g1 * e4 + g2 * e1
X * e3 == g1 * e1 + g2 * e4
X * e4 == g1 * e2 + g2 * e3

# our case

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )

## consider two identical harmonic oscillators each coupled to one of the sites
## we truncate to second eigenstate


## Hamiltonian

exc = [tensor(one, zero, zero, zero), tensor(zero, one, zero, zero)]
    
HI = 0    
for i in range(N):
    HI += X * exc[i] * exc[i].dag()