#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  5 21:13:34 2017

@author: fcm
"""

# SPIN-BOSON

delta = 0
landa = 10
w1 = 12
w2 = 20

a = destroy(2)

a1 = tensor(a,qeye(2))
a2 = tensor(qeye(2),a)

psi0 = 1/sqrt(2) * (zero + one)
rhos = ket2dm(psi0)
rho0 = tensor(rhos, zero * zero.dag(), zero * zero.dag())
H = delta / 2 * sigmaz()
HI = landa * tensor(sigmax(), (a1.dag() + a1 + a2.dag() + a2))
HB = w1 * a1.dag() * a1 + w2 * a2.dag() * a2

H = tensor(H, tensor(qeye(2), qeye(2))) + HI + tensor(qeye(2), HB)
                      
times = linspace(0.0, 50.0, 300.0)
result = mesolve(H, rho0, times, [], [tensor(sigmaz(), tensor(qeye(2), qeye(2)))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
# savefig('sigmaz.png')
show()
