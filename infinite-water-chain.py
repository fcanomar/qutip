#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 25 11:59:00 2018

@author: fcm
"""

from ase.visualize import view
from ase import Atoms
from ase.units import Hartree, Bohr
from ase.constraints import FixBondLengths
from ase.calculators.tip3p import TIP3P, rOH, angleHOH
from ase.optimize import QuasiNewton
from ase.md import Langevin
import ase.units as units
from ase.io.trajectory import Trajectory
from ase.build import nanotube
import numpy as np
from gpaw.external import ExternalPotential
from gpaw import GPAW
import plot_potential_xy as plxy
import matplotlib.pyplot as plt
from pylab import savefig


##########################################################################################
# FIRST I CREATE AND CALCULATE POTENTIAL FROM THE NANOTUBE
##########################################################################################

# source plot_potential_xy.py first

# =============================================================================
ntb = nanotube(6, 0, length=20)
ntb.set_cell((10, 10+0.001 , 27.5 + 5))
ntb.center()
ntb.calc = GPAW(gpts=(40, 40, 110))
 
# ntb.calc = GPAW(gpts=(20, 20, 55))
 
#  ------> at least run and stop to get next line going
ntb.get_potential_energy()
 
potential = ntb.calc.get_electrostatic_potential()
 
plxy.plot_potential_xy(ntb, tag='60-')
 
ntb.calc.attach(ntb.calc.write, 'nanotube-potential.gpw')
 
view(ntb)
# =============================================================================


################################################################################
# View potential 
################################################################################

# Plano perpendicular

sys = ntb
tag = ''
vfrom = None
vto = None

ax = sys.cell[0,0]
ay = sys.cell[1,1]

v = potential[0:79,0:79,0:219]

nx, ny, nz = v.shape

x = np.linspace(0, ax, nx, endpoint=False)
y = np.linspace(0, ay, ny, endpoint=False)

v_xy = v[slice(0,nx),slice(0,ny),nz//2] # Plane XY

plt.figure(figsize=(13, 10))

if vfrom == None: 
    vfrom = v_xy.min()
    
if vto == None:
    vto = v_xy.max()
    
colorbar = np.linspace(vfrom, vto, 15)

plt.contourf(x, y, v_xy.T, colorbar)
plt.colorbar()
plt.title('Electrostatic Potential (eV) [' + tag + ']')

if tag != None:
    savefig(tag + '-potential-scale-xy.png')

# Plano Longitudinal

ax = sys.cell[0,0]
ay = sys.cell[1,1]
az = sys.cell[2,2]

v = potential[0:79,0:79,0:219]

nx, ny, nz = v.shape

x = np.linspace(0, ax, nx, endpoint=False)
z = np.linspace(0, az, nz, endpoint=False)

v_xz = v[slice(0,nx),ny//2,slice(0,nz)] # Plane XY

plt.figure(figsize=(13, 10))

if vfrom == None: 
    vfrom = v_xz.min()
    
if vto == None:
    vto = v_xz.max()
    
colorbar = np.linspace(vfrom, vto, 15)

plt.contourf(x, z, v_xz.T, colorbar)
plt.colorbar()
plt.title('Electrostatic Potential (eV) [' + tag + ']')

if tag != None:
    savefig(tag + '-potential-scale-xz.png')


##########################################################################################
# I DO MD OF WATER MOLECULES USING GPAW IN THE NANOTUBE POTENTIAL
##########################################################################################


# Set up water box at 20 deg C density

# ntb, ntb.calc = restart('xyz.gpw')

x = angleHOH * np.pi / 180 / 2
pos = [[0, 0, 0],
       [0, rOH * np.cos(x), rOH * np.sin(x)],
       [0, rOH * np.cos(x), -rOH * np.sin(x)]]
atoms = Atoms('OH2', positions=pos)

# atoms.set_cell((10, 10 + 0.0001, 5)) # after repeat makes length 35
atoms.set_cell((10, 10+0.001 , 2.75)) # after repeat makes length 35

# atoms.center()

N = 10 # N-atoms chains
atoms = atoms.repeat((1, 1, N))

# atoms.set_pbc(False)

# atoms.set_cell((10, 10+0.001 , 27.5/2 + 5))

atoms.set_cell((10, 10+0.001 , 27.5 + 5))
atoms.center()

# atoms.set_pbc((False, False, True))
atoms.set_pbc((False))

# break simmetry

####### ---->

# RATTLE-type constraints on O-H1, O-H2, H1-H2.

atoms.constraints = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                                   for i in range(N)
                                   for j in [0, 1, 2]])

####### <----


# First Equilibrate Using Cheap Method Tip3p Water Potential

#tag = 'water-chain-from-box-md-tip3p-nanotube-potential'
#
#atoms.calc = TIP3P(rc=4.5)
#md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
#              friction=0.01, logfile=tag + '.log')
#
#traj = Trajectory(tag + '.traj', 'w', atoms)
#md.attach(traj.write, interval=1)
#md.run(4000)


# Equilibrate further using GPAW and Nanotube External Potential

# from ase.visualize import view
# view(atoms)

#from gpaw.external import ConstantPotential
#
#class NanotubePotential(ExternalPotential):
#    def calculate_potential(self, gd):
#        self.vext_g = potential[0:79,0:79,0:139] # drop one element in each dimension to fit given potential 

class NanotubePotential(ExternalPotential):
    def calculate_potential(self, gd):
        self.vext_g = potential[0:79,0:79,0:219] # drop one element in each dimension to fit given potential 


tag = 'water-chain-from-box-md-gpaw-nanotube-potential-ten'
# atoms.calc = GPAW(external=NanotubePotential(),gpts=(122, 122, 422))
# calc = GPAW(external=NanotubePotential(), gpts=(63, 63, 192), mode='lcao', symmetry={'point_group': False})

# calc = GPAW(gpts=(40,40,110), mode='lcao', symmetry={'point_group': False})

# calc = GPAW(gpts=(40,40,110), mode='lcao', symmetry={'point_group': False})

calc = GPAW(external=NanotubePotential(), gpts=(40, 40, 110), mode='lcao')

# calc = GPAW(external=NanotubePotential(), gpts=(40,40,110), mode='lcao', symmetry={'point_group': False})


# calc = GPAW(external=NanotubePotential(), gpts=(32, 32, 96), mode='lcao')

atoms.set_calculator(calc)

md = Langevin(atoms, 1 * units.fs, temperature=300 * units.kB,
              friction=0.01, logfile=tag + '.log')

traj = Trajectory(tag + '.traj', 'w', atoms)
md.attach(traj.write, interval=1)
md.run(4000)



