#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 14:39:41 2017

@author: fcm
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 14:09:29 2017

@author: fcm
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 20 17:31:10 2017

Nature Dimer Coupled to Bath - Pauli Matrices

@author: fcm
"""

from qutip import *
from pylab import *
from scipy import *
from math import factorial
import sympy
import os

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)

# working directory

os.chdir('/Users/fcm/qutip/figures/exciton')


# state space

# |0> and |1> states 

N = 2

zero = basis(2,0)
one = basis (2,1)

e1 = tensor(one, zero)
e2 = tensor(zero, one)

a1 = tensor(destroy(2),qeye(2))
a2 = tensor(qeye(2), destroy(2))

e = [e1,e2]


# parameters

eps = [10, 130]

J = numpy.array([[53.5, 53.5],[53.5, 53.5]])


# spectral densities

ld = 35
wa = 0.57
wb = 1.9
S1 = 0.12
S2 = 0.22

def J0(w):
    J0 = (ld * (1000 * w**5 * exp(-sqrt(w/wa)) + 4.3 * w**5 * exp(-sqrt(w/wb))))/(factorial(9) * (1000 * wa**5 + 4.3 * wb**5))
    return J0

def JJ(w):
    JJ = J0(w) + S1 * wa**2 * ((w - wa)==0) + S2 * wb**2 * ((w - wb)==0)
    return JJ


# mode displacement operator

X = sqrt(JJ(wa)) * ( a1 + a1.dag() ) + sqrt(JJ(wb)) * ( a2 + a2.dag() )


# EXCITON BASIS

w = [wa, wb]

A = [a1, a2]

e1 = tensor(one, zero)
e2 = tensor(zero, one)

e =[e1, e2]


# HAMILTONIAN IN SITE BASIS

HS = 0
for i in range(N):
    HS += eps[i] * e[i] * e[i].dag()
    for j in range(N):
            if i <> j :
                HS += J[i,j] * e[i] * e[j].dag()
    

# excitons

exc1 = HS.eigenstates()[1][0]
E1 = HS.eigenstates()[0][0]
exc2 = HS.eigenstates()[1][3]
E2 = HS.eigenstates()[0][3]

HS * exc1 == E1 * exc1
HS * exc2 == E2 * exc2

exc = [exc1, exc2]
E = [E1, E2]  

# |1> ie. |10> coefficients

c11 = exc1.dag() * e1
c12 = exc2.dag() * e1
              
# |2> ie. |01> coefficients
              
c21 = exc1.dag() * e2              
c22 = exc2.dag() * e2       
              
C = array([[c11, c12],[c21, c22]])              

# single excitation vectors in exciton basis              
              
e1 == c11 * exc1 + c12 * exc2
e2 == c21 * exc1 + c22 * exc2

# S and Q

S11 = sqrt(sqrt(JJ(wa))/wa)
S12 = sqrt(sqrt(JJ(wb))/wb)
S21 = S11
S22 = S12

S = array([[S11, S12], [S21, S22]])

def Q(n,m):
    Q = 0
    for i in range(N):
        for k in range(2):
            Q = sqrt(S[i,k]) * w[k] * C[i,n] * C[i,m] * (A[k] + A[k].dag())
    return Q

# HAMILTONIAN IN EXCITON BASIS

# I now use excitons as a basis such that

exc1 = tensor(zero, one)
exc2 = tensor(one, zero)

exc = [exc1, exc2]

# Hamiltonian

HSE = E1 * exc1 * exc1.dag() + E2 * exc2 * exc2.dag()
  
HIE = 0
for n in range(N):
    for m in range(N):
        HIE += tensor(Q(n,m), exc[n] * exc[m].dag())
        
HB =   2 * (wa * a1.dag() * a1 + wb * a2.dag() * a2)
    
HE = tensor(HSE, tensor(qeye(2), qeye(2))) + HIE + tensor(tensor(qeye(2), qeye(2)),HB)

###################################################################################


# time evolution starting in one localized excitation

psi0 = 1/sqrt(2)*(exc1 + exc2)
rhos = ket2dm(psi0)
#rhob = thermal_dm(4,2)

rhob = tensor(thermal_dm(2,2), thermal_dm(2,2))


#rhob = ket2dm(tensor(zero, zero))


rho0 = tensor(rhos, rhob)

# sigmaz

times = linspace(0.0, 10000, 10000.0)
result = mesolve(HE, rho0, times, [], [tensor(exc1 * exc1.dag(), qeye(2), qeye(2))])


plt.plot(times, result.expect[0])
plt.ylabel('Sigma_z')
plt.xlabel('Time')
savefig('sigmaz_localized.png')
show()



# coherence in rdm

result = mesolve(HE, rho0, times, [], [])
l = len(result.states)

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[0,0]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_00 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho00_rdm_localized.png')
show() 


x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,1]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_11 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show() 

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[1,2].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_12 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho01_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,1].imag
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_21 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho10_rdm_localized.png')
show()  

x = []
for i in range(l):
    rdm = result.states[i].ptrace([0,1])
    s = rdm[2,2]
    x.append(s)

plt.plot(times, x)
plt.ylabel('rho_22 (reduced density matrix)')
plt.xlabel('Time')
savefig('rho11_rdm_localized.png')
show()   
  



## time evolution starting in triplet state
#
#psi0 = 1/sqrt(2) * (tensor(zero,one) + tensor(one,zero))
#rhos = ket2dm(psi0)
##rhob = thermal_dm(4,2)
#rhob = ket2dm(tensor(one, one))
#rho0 = tensor(rhos, rhob)
#
## sigmaz
#
#times = linspace(0.0, 10.0e-1, 600.0)
#result = mesolve(H, rho0, times, [], [tensor(e1 * e1.dag(),qeye(2),qeye(2))])
#
#
#plt.plot(times, result.expect[0])
#plt.ylabel('Sigma_z')
#plt.xlabel('Time')
#savefig('sigmaz_localized.png')
#show()
#
#
#
## coherence in rdm
#
#result = mesolve(H, rho0, times, [], [])
#l = len(result.states)
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[0,0]
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_00 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho00_rdm_localized.png')
#show() 
#
#
#result = mesolve(H, rho0, times, [], [])
#l = len(result.states)
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[1,1]
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_11 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho11_rdm_localized.png')
#show() 
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[1,2].imag
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_12 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho01_rdm_localized.png')
#show()  
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[2,1].imag
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_21 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho10_rdm_localized.png')
#show()  
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[2,2]
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_22 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho11_rdm_localized.png')
#show()   
#  
#  
## time evolution starting in singlet state
#
#psi0 = 1/sqrt(2) * (tensor(zero,one) - tensor(one,zero))
#rhos = ket2dm(psi0)
##rhob = thermal_dm(4,2)
#rhob = ket2dm(tensor(one, one))
#rho0 = tensor(rhos, rhob)
#
## sigmaz
#
#times = linspace(0.0, 10.0e-1, 600.0)
#result = mesolve(H, rho0, times, [], [tensor(e1 * e1.dag(),qeye(2),qeye(2))])
#
#
#plt.plot(times, result.expect[0])
#plt.ylabel('Sigma_z')
#plt.xlabel('Time')
#savefig('sigmaz_localized.png')
#show()
#
#
#
## coherence in rdm
#
#result = mesolve(H, rho0, times, [], [])
#l = len(result.states)
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[0,0]
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_00 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho00_rdm_localized.png')
#show() 
#
#
#result = mesolve(H, rho0, times, [], [])
#l = len(result.states)
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[1,1]
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_11 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho11_rdm_localized.png')
#show() 
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[1,2].imag
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_12 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho01_rdm_localized.png')
#show()  
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[2,1].imag
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_21 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho10_rdm_localized.png')
#show()  
#
#x = []
#for i in range(l):
#    rdm = result.states[i].ptrace([0,1])
#    s = rdm[2,2]
#    x.append(s)
#
#plt.plot(times, x)
#plt.ylabel('rho_22 (reduced density matrix)')
#plt.xlabel('Time')
#savefig('rho11_rdm_localized.png')
#show()   
#  