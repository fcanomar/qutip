
# coding: utf-8

# # CUSTOM Two-Level System Coupled to a Harmonic Oscillator
# 
# Based on Section 3.4 of Density Matrices Notes of the Course "Chem 542 Advanced Graduate Quantum Mechanics" University of Illinois at Urbana Campaign, available at http://www.scs.illinois.edu/mgweb/Course_Notes/chem349/classnotes.html


from qutip import *
from pylab import *

# show whole vectors 
import numpy
numpy.set_printoptions(threshold=numpy.nan)


# ## Space of States
# 
# States of the Two-Level System (TLS):
# |0> or -1/2
# |1> or +1/2
# 
# States of the Harmonic Oscillator (HO):
# |n> with n=1...N
# 
# A convenient basis for the composite system (TLS+HO) is {|s>|n>} ie. {|s> x |n>} or similarly {|n>|s>} ie. {|n> x |s>},
# with s=0,1 and n=0...N
# 
# I use the second one, {|n>|s>} or, in short, {|ns>}
# 
# I define these states :


# Two-Level System

E = 10

zero = basis(2,0) # |s> = |0>
one = basis(2,1) # |1>

# Harmonic Oscillator

N = 3 # for example truncate at level 3
w = 0.1
# I set h_bar equal to one

a = destroy(N)

hzero = basis(N,0) # |n> = |0>
hone = basis(N,1) # |1>
htwo = basis(N,2) # |2>


# Composite System : Two-Level System + Harmonic Oscillator

V=1

v1 = tensor(hzero, zero) # |ns> = |00>
v2 = tensor(hzero, one) # |01>
v3 = tensor(hone, zero) # |10>
v4 = tensor(hone, one) # |11>
v5 = tensor(htwo, zero) # |20>
v6 = tensor(htwo, one) # |21>


# Some preliminary exploration of our states :

# ### Two-Level System



# ### Harmonic Oscillator :



a.dag() * hone == sqrt(2) * htwo



# ## Hamiltonian
# The Hamiltonian is :

# H = 0|0><0| + E|1><1| + V * ( a.dag() * |0><1| + a * |1><0| ) + w * (n + 0.5)

# ie. 

# H = 0 * I x |0><0| + E * I x |1><1| + V * ( a.dag() x |0><1| + a x |1><0| ) + I x (w * (a_dag * a + 0.5))


H = 0 * tensor(qeye(3),zero * zero.dag()) + E * tensor(qeye(3), one * one.dag()) + V * ( tensor(a.dag(),zero * one.dag()) + tensor(a, one * zero.dag())) + tensor( w * (a.dag() * a + .5), qeye(2))
H


# ## Time Evolution
# 
# Initial State :


psi0 = tensor(1/sqrt(2) * (zero + one), hzero)
psi0



rho0 = ket2dm(psi0)
rho0



rho0 == psi0 * psi0.dag()



# times = linspace(0.0, 10.0, 20.0)
# result = mesolve(H, rho0, times, [], [])
# result.states

## no resuelve por incompatibilidad de objetos


# We limit our example to N=2. TLS and HO are in resonace so E = hw.


E = w

# Space of States :

# Harmonic Oscillator

N = 2 # for example truncate at level 3
w = 0.1
# I set h_bar equal to one

a = destroy(N)

hzero = basis(N,0) # |n> = |0>
hone = basis(N,1) # |1>


# Composite System : Two-Level System + Harmonic Oscillator

V=1

v1 = tensor(hzero, zero) # |ns> = |00>
v2 = tensor(hzero, one) # |01>
v3 = tensor(hone, zero) # |10>
v4 = tensor(hone, one) # |11>

# Hamiltonian :

H = 0 * tensor(qeye(2),zero * zero.dag()) + E * tensor(qeye(2), one * one.dag()) + V * ( tensor(a.dag(),zero * one.dag()) + tensor(a, one * zero.dag())) + tensor( w * (a.dag() * a + .5), qeye(2))
H

H1 = 0 * tensor(qeye(2),zero * zero.dag()) + E * tensor(qeye(2), one * one.dag()) + V * tensor(a.dag() + a,zero * one.dag() + one * zero.dag()) + tensor( w * (a.dag() * a + .5), qeye(2))
H1


H.eigenstates()



psi0 = tensor(1/sqrt(2) * (zero + one), hzero)
rho0 = ket2dm(psi0)
rho0


# I calculate theoretical time evolution and compare to results obtained using master equation solver. I compare a diagonal and one off-diagonal terms of the density matrices


times = linspace(0.0, 30.0, 80.0)
psi0 = tensor(hzero, 1/sqrt(2) * (zero + one))
rho0 = ket2dm(psi0)
result = mesolve(H, ket2dm(psi0), times, [], [])
l = len(result.states)


r11 = []
for t in times:
    s = 1/4. * cos(2 * V * t) + 1/4.
    r11.append(s)

x11 = []
for i in range(l):
    s = result.states[i][1,1]
    x11.append(s)

[array(r11), array(x11)]


max(array(r11) - array(x11)) < 0.001


# Difference between both temporal sequences is less than 0.001.


r01 = []
for t in times:
    s = 1/4. * exp(1j * t * (w - V)) + 1/4. * exp(1j * t * (w + V))
    r01.append(s)
    
x01 = []
for i in range(l):
    s = result.states[i][0,1]
    x01.append(s)

max(array(r01) - array(x01)) < 0.001


# I obtain the reduced density matrices for the system by tracing out environment degrees of freedom

# Reduced Density Matrix

rho0.ptrace(1)

# Reduced Density Matrix Time Evolution

rdm = []
for i in range(l):
    s = result.states[i].ptrace(1)
    rdm.append(s)

rdm01 = []
for i in range(l):
    s = result.states[i][0,1]
    rdm01.append(s)
    
rt12 = []
for t in times:
    s = 1/4. * exp(1j * t * (w - V)) + 1/4. * exp(1j * t * (w + V))
    rt12.append(s)
    
max(array(rdm01) - array(rt12)) < 0.001


plt.plot(times, rdm01)
plt.ylabel('Rho_12')
plt.xlabel('Time')
show()




# Due to the coupling to the harmonic oscillator the reduced system's phase oscillates. 
# 
# A single harmonic oscillator cannot make the system dephase completely. It makes it oscillate between a pure state and a partially dephased state. 
